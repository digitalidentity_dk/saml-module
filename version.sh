#!/bin/bash
VERSION="2.4.5"

mv target/saml-module.jar target/saml-module-$VERSION.jar
/usr/bin/sha1sum target/saml-module-$VERSION.jar > target/saml-module-$VERSION.jar.sha1

mv target/saml-module-sources.jar target/saml-module-$VERSION-sources.jar
/usr/bin/sha1sum target/saml-module-$VERSION-sources.jar > target/saml-module-$VERSION-sources.jar.sha1

cp pom.xml target/saml-module-$VERSION.pom
/usr/bin/sha1sum target/saml-module-$VERSION.pom > target/saml-module-$VERSION.pom.sha1
