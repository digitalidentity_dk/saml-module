package dk.digitalidentity.samlmodule.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.SamlIdentityProviderProvider;
import dk.digitalidentity.samlmodule.model.TokenUser;
import dk.digitalidentity.samlmodule.model.enums.NSISLevel;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.util.Constants;
import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.SessionException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SAMLFilter implements Filter {
	private DISAML_Configuration configuration;
	private DISAML_SessionHelper sessionHelper;
	private SamlIdentityProviderProvider identityProviderProvider;

	@Override
	@SneakyThrows
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
		if (log.isTraceEnabled()) {
			log.trace("SAMLFilter invoked");
		}

		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpServletRequest request = (HttpServletRequest) servletRequest;

		// get the requested path
		String requestedPath = request.getRequestURI();
		if (log.isTraceEnabled()) {
			log.trace("Raw requested path    : '" + (requestedPath != null ? requestedPath : "<null>") + "'");
		}

		if (requestedPath == null) {
			unauthorized(response, "getRequestURI was null", requestedPath);
			return;
		}

		// strip contextPath
		if (StringUtils.hasLength(configuration.getProxy().getContextPath()) && requestedPath.startsWith(configuration.getProxy().getContextPath())) {
			requestedPath = requestedPath.substring(configuration.getProxy().getContextPath().length());
		}

		// trying to access the root "/" contextPath will return ""
		if ("".equals(requestedPath)) {
			requestedPath = "/";
		}

		if (log.isTraceEnabled()) {
			log.trace("Reqested path: " + requestedPath);
		}

		// if trying to access a SAML page -> ALLOW
		if (requestedPath.startsWith(configuration.getPages().getPrefix())) {
			ensureRequestedNsisLevelOnSession();
			authorized(filterChain, servletRequest, servletResponse, requestedPath, "SAML page");
			return;
		}

		// if trying to access a Discovery page while feature enabled -> ALLOW
		if (configuration.getIdp().isDiscovery() && requestedPath.equals(configuration.getPages().getChooseIdentityProvider())) {
			ensureRequestedNsisLevelOnSession();
			authorized(filterChain, servletRequest, servletResponse, requestedPath, "Discovery page");
			return;
		}

		// if trying to access a non-secured page? -> ALLOW
		for (String page : configuration.getPages().getNonsecured()) {
			if (page.endsWith("**") && requestedPath.startsWith(page.substring(0, page.length() - 2))) {
				authorized(filterChain, servletRequest, servletResponse, requestedPath, "Non-secured suffix path");
				return;
			}
			else if (page.startsWith("**") && requestedPath.endsWith(page.substring(2, page.length()))) {
				authorized(filterChain, servletRequest, servletResponse, requestedPath, "Non-secured postfix file");
				return;
			}
			else if (requestedPath.equals(page)) {
				authorized(filterChain, servletRequest, servletResponse, requestedPath, "Non-secured page");
				return;
			}
		}

		// if trying to access a non-secured page? -> ALLOW
		if (isMatchingConfiguredEndpoints(configuration.getPages().getNonsecured(), requestedPath)) {
			authorized(filterChain, servletRequest, servletResponse, requestedPath, "Non-secured endpoint");
			return;
		}

		// compute required NSIS Level for the given endpoint
		// default is Substantial, goes from low->high so highest required matching endpoint is used
		NSISLevel requiredNsisLevel = NSISLevel.NONE;
		if (configuration.getPages().isNsisRequired()) {
			requiredNsisLevel = configuration.getPages().getNsisRequiredDefaultLevel();
			
			if (isMatchingConfiguredEndpoints(configuration.getPages().getNsisNotRequired(), requestedPath)) {
				// special case - NSIS is enabled, but we have a series of endpoints where NSIS is NOT required
				requiredNsisLevel = NSISLevel.NONE;
			}
			else if (isMatchingConfiguredEndpoints(configuration.getPages().getNsisHighRequired(), requestedPath)) {
				requiredNsisLevel = NSISLevel.HIGH;
			}
			else if (isMatchingConfiguredEndpoints(configuration.getPages().getNsisSubstantialRequired(), requestedPath)) {
				requiredNsisLevel = NSISLevel.SUBSTANTIAL;
			}
			else if (isMatchingConfiguredEndpoints(configuration.getPages().getNsisLowRequired(), requestedPath)) {
				requiredNsisLevel = NSISLevel.LOW;
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Required NSIS Level evaluated to " + requiredNsisLevel.toClaimValue());
		}

		sessionHelper.setBoolean(SessionConstant.BROKERING_ENABLED, isMatchingConfiguredEndpoints(configuration.getPages().getBrokeredEndpoints(), requestedPath));
		if (log.isDebugEnabled()) {
			log.debug("Brokering " + isMatchingConfiguredEndpoints(configuration.getPages().getBrokeredEndpoints(), requestedPath));
		}

		// check if the requester is already authenticated
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated() && !(authentication instanceof AnonymousAuthenticationToken) && authentication.getDetails() instanceof TokenUser) {
			// user is already authenticated, determine level

			// since we are already authenticated, we default to assume that the assertion has granted authentication, but no nsis level
			NSISLevel suppliedNSISLevel = NSISLevel.NONE;

			// fetch Level Of Assurance (if present)
			TokenUser tokenUser = (TokenUser) authentication.getDetails();
			Object loa = tokenUser.getAttributes().get(Constants.LEVEL_OF_ASSURANCE);

			if (loa instanceof String) {
				NSISLevel nsisLevelFromAssertion = NSISLevel.fromClaimValue((String) loa);
				if (nsisLevelFromAssertion != null) {
					suppliedNSISLevel = nsisLevelFromAssertion;
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("NSIS Level from Assertion was evaluated to " + suppliedNSISLevel.toClaimValue());
			}

			// Compare required and supplied nsis levels
			if (requiredNsisLevel.equalOrLesser(suppliedNSISLevel)) {
				// The user is already logged in to an appropriate NSIS level
				authorized(filterChain, servletRequest, servletResponse, requestedPath, "Already authenticated");
			}
			else {
				// step up
				// redirect to SAML flow
				if (log.isDebugEnabled()) {
					log.debug("User was authenticated but step up is needed to access requested path");
				}

				initiateLogin(request, response, requestedPath, requiredNsisLevel);
			}
		}
		else {
			// if the user not authenticated -> redirect to SAML login flow, remember desired page
			if (log.isDebugEnabled()) {
				log.debug("DENIED  Request. Reason: Not authenticated. Redirecting.");
			}

			// redirect to SAML flow
			initiateLogin(request, response, requestedPath, requiredNsisLevel);
		}
	}

	private void ensureRequestedNsisLevelOnSession() throws SessionException {
		if (configuration.getPages().isNsisRequired()) {
			String nsisLevel = sessionHelper.getString(SessionConstant.NEEDED_NSIS_LEVEL);
			if (nsisLevel == null) {
				sessionHelper.setString(SessionConstant.NEEDED_NSIS_LEVEL, configuration.getPages().getNsisRequiredDefaultLevel().toClaimValue());
			}
		}		
	}

	private void initiateLogin(HttpServletRequest request, HttpServletResponse response, String requestedPath, NSISLevel requiredNsisLevel) throws IOException, SessionException {
		saveDesiredPage(requestedPath);
		sessionHelper.setString(SessionConstant.NEEDED_NSIS_LEVEL, requiredNsisLevel.toClaimValue());
		StringBuilder sb = new StringBuilder();
		sb.append(configuration.getProxy().getContextPath()).append(configuration.getPages().getPrefix()).append(configuration.getPages().getLoginMetadataPath());

		// if we have discovery enabled we sometimes should append an IdP for ease of use
		if (configuration.getIdp().isDiscovery()) {
			String idp = request.getParameter("idp");
			if (StringUtils.hasLength(idp)) {
				// if the incoming request has an "idp" query parameter then we should resend it in our new request
				if (log.isDebugEnabled()) {
					log.debug("Appending IdP request parameter to new redirect");
				}

				sb.append("?idp=").append(idp);
			}
			else if (identityProviderProvider != null && sessionHelper.getString(SessionConstant.CHOSEN_IDENTITY_PROVIDER) != null) {
				// if the user has already chosen an IdP we will reuse that until it is deleted on logout/error
				String entityId = sessionHelper.getString(SessionConstant.CHOSEN_IDENTITY_PROVIDER);
				IdentityProvider chosenIdentityProvider = identityProviderProvider.getByEntityId(entityId);
				if (chosenIdentityProvider != null) {
					sb.append("?idp=").append(chosenIdentityProvider.getEntityId());
				}
			}
		}

		response.sendRedirect(sb.toString());
	}

	private void saveDesiredPage(String requestedPath) {
		try {
			// do not store browser attempts to access map, js, css and ico files
			if (!requestedPath.endsWith(".map") && !requestedPath.endsWith(".js") && !requestedPath.endsWith(".css") && !requestedPath.endsWith(".ico")) {
				// remember page that requester was trying to access
				sessionHelper.setString(SessionConstant.DESIRED_PAGE, configuration.getProxy().getContextPath() + requestedPath);

				if (log.isDebugEnabled()) {
					log.debug(SessionConstant.DESIRED_PAGE + " successfully set to path: '" + configuration.getProxy().getContextPath() + requestedPath + "'");
				}
			}
		}
		catch (SessionException ignored) {
			; // we try to set desired page, if it fails we continue on with authentication
		}
	}

	private boolean isMatchingConfiguredEndpoints(List<String> configuredEndpoints, String requestedPath) {
		if (configuredEndpoints == null || configuredEndpoints.isEmpty()) {
			return false;
		}

		for (String page : configuredEndpoints) {
			if (page.endsWith("**") && requestedPath.startsWith(page.substring(0, page.length() - 2))) {
				return true;
			}
			else if (page.startsWith("**") && requestedPath.endsWith(page.substring(2))) {
				return true;
			}
			else if (requestedPath.equals(page)) {
				return true;
			}
		}

		return false;
	}

	public void setServices(DISAML_Configuration configuration, DISAML_SessionHelper sessionHelper, @Nullable SamlIdentityProviderProvider identityProviderProvider) {
		this.configuration = configuration;
		this.sessionHelper = sessionHelper;
		this.identityProviderProvider = identityProviderProvider;
	}

	private static void authorized(FilterChain filterChain, ServletRequest servletRequest, ServletResponse servletResponse, String path, String reason) throws IOException, ServletException {
		if (log.isDebugEnabled()) {
			log.debug("ALLOWED Request. Reason: '" + reason + "'. Path: '" + (path != null ? path : "<null>") + "'");
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	private static void unauthorized(HttpServletResponse response, String reason, String path) throws IOException {
		log.debug("DENIED  Request. Reason: '" + reason + "'. Path: '" + (path != null ? path : "<null>") + "'");
		response.sendError(401, reason);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		;
	}

	@Override
	public void destroy() {
		;
	}
}
