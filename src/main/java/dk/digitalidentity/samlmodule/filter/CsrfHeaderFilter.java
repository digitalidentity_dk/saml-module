package dk.digitalidentity.samlmodule.filter;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CsrfHeaderFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws ServletException, IOException {
        Object tok = req.getAttribute("_csrf");

        if (tok != null && tok instanceof CsrfToken) {
            resp.setHeader("X-CSRF-TOKEN", ((CsrfToken) tok).getToken());
            resp.setHeader("X-CSRF-PARAM", ((CsrfToken) tok).getParameterName());
            resp.setHeader("X-CSRF-HEADER", ((CsrfToken) tok).getHeaderName());
        }

        chain.doFilter(req, resp);
    }
}