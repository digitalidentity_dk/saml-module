package dk.digitalidentity.samlmodule.model;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class SamlGrantedAuthority implements GrantedAuthority {
	private static final long serialVersionUID = -6125386536952311230L;

	private String authority;
	private List<Constraint> constraints;
	private String scope;

	public SamlGrantedAuthority(String role) {
		this.authority = role;
	}

	public SamlGrantedAuthority(String role, String scope) {
		this.authority = role;
		this.scope = scope;
	}

	public SamlGrantedAuthority(String role, List<Constraint> constraints) {
		this.authority = role;
		this.constraints = constraints;
	}

	@Getter
	@Builder
	@Setter
	@EqualsAndHashCode
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Constraint {
		private String constraintType;
		private String constraintValue;
	}
}
