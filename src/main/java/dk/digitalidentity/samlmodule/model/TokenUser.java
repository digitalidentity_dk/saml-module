package dk.digitalidentity.samlmodule.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

@Getter
@Setter
@Builder
public class TokenUser implements UserDetails {
	public static final String ATTRIBUTE_NAME = "SubjectNameID-name";
	public static final String ATTRIBUTE_UUID = "SubjectNameID-uuid";

	private static final long serialVersionUID = 1L;
	private String cvr;
	private Collection<SamlGrantedAuthority> authorities;
	private String username;
	private String rawToken;
	private Map<String, Object> attributes;

	public String getAndClearRawToken() {
		String rawTokenCopy = rawToken;
		rawToken = null;

		return rawTokenCopy;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
