package dk.digitalidentity.samlmodule.model.enums;

public enum AssertionLogging {
	FULL, COMPACT, NONE
}
