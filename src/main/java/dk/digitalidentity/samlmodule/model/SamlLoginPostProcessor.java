package dk.digitalidentity.samlmodule.model;

public interface SamlLoginPostProcessor {
	void process(TokenUser tokenUser);
}
