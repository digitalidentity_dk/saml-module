package dk.digitalidentity.samlmodule.model;

import java.util.List;

public interface SamlIdentityProviderProvider {
	/**
	 * Used by the saml-module to fetch all configured Identity Providers
	 * @return a non-null list of all configured identity providers
	 */
	List<IdentityProvider> getIdentityProviders();

	/**
	 * Used by the saml-module to fetch an Identity Provider by their entityID
	 * @param entityId a non-null and non-empty string
	 * @return Returns either the matching IdentityProvider or NULL if no IdP matches
	 */
	IdentityProvider getByEntityId(String entityId);
}
