package dk.digitalidentity.samlmodule.model;

import net.shibboleth.utilities.java.support.resource.Resource;

import java.io.IOException;

public class InMemoryResource extends org.springframework.security.util.InMemoryResource implements Resource {

	public InMemoryResource(byte[] source) {
		super(source);
	}

	@Override
	public Resource createRelativeResource(String relativePath) throws IOException {
		return null;
	}
	
	@Override
	public long lastModified() throws IOException {
		return 0;
	}
}
