package dk.digitalidentity.samlmodule.model;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdentityProvider {
	private String entityId;
	private String metadata;
	private String cvr;
	private boolean contextClassRefEnabled;
	private boolean requirePersonProfile;

	public IdentityProvider(DISAML_Configuration configuration) {
		this.entityId = Constants.CONFIGURED_IDENTITY_PROVIDER;
		this.metadata = configuration.getIdp().getMetadataLocation();
		this.contextClassRefEnabled = configuration.getIdp().isContextClassRefEnabled();
		this.requirePersonProfile = configuration.getIdp().isRequirePersonProfile();
		this.cvr = null;
	}
}
