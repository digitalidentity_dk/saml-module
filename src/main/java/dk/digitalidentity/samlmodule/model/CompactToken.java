package dk.digitalidentity.samlmodule.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompactToken {
	private String nameId;
	private String roles;
	private String cvr;
}
