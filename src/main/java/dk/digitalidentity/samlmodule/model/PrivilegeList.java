package dk.digitalidentity.samlmodule.model;

import java.util.ArrayList;
import java.util.List;


import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "privilegeGroup" })
// TODO: NemLog-in bruger den udkommenterede, og KOMBIT bruger den anden, det skal lige fixes på et tidspunkt
//@XmlRootElement(name = "PrivilegeList", namespace="http://digst.dk/oiosaml/basic_privilege_profile")
@XmlRootElement(name = "PrivilegeList", namespace="http://itst.dk/oiosaml/basic_privilege_profile")
public class PrivilegeList {

	@XmlElement(name = "PrivilegeGroup", required = true)
	protected List<PrivilegeList.PrivilegeGroup> privilegeGroup;

	public List<PrivilegeList.PrivilegeGroup> getPrivilegeGroup() {
		if (privilegeGroup == null) {
			privilegeGroup = new ArrayList<>();
		}

		return this.privilegeGroup;
	}

	@Getter
	@Setter
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "privilege", "constraint" })
	public static class PrivilegeGroup {

		@XmlElement(name = "Privilege", required = true)
		protected List<PrivilegeList.Privilege> privilege;

		public List<PrivilegeList.Privilege> getPrivilege() {
			if (privilege == null) {
				privilege = new ArrayList<>();
			}

			return this.privilege;
		}

		@XmlElement(name = "Constraint", required = true)
		protected List<PrivilegeList.Constraint> constraint;

		public List<PrivilegeList.Constraint> getConstraint() {
			if (constraint == null) {
				constraint = new ArrayList<>();
			}

			return this.constraint;
		}

		@XmlAttribute(name = "Scope")
		protected String scope;
	}

	@Getter
	@Setter
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class Privilege {

		@XmlValue
		protected String value;
	}
	
	@Getter
	@Setter
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class Constraint {

		@XmlValue
		protected String value;

		@XmlAttribute(name = "Name")
		protected String name;
	}
}