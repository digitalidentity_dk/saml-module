package dk.digitalidentity.samlmodule.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.filter.CsrfHeaderFilter;
import lombok.extern.slf4j.Slf4j;

// The normal "WebSecurityConfigurerAdapter" Uses Order=100, and it must be unique.
// Applications might extend "WebSecurityConfigurerAdapter" on top of having saml-module installed,
// so we set to 105 to avoid conflicts
@Order(105)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private DISAML_Configuration configuration;

	@Bean
	public UserDetailsService userDetailsService() {
		// empty UserDetailsService to make Spring Security happy :)
		return new InMemoryUserDetailsManager();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("Configuring CSRF and CORS preflight.");
		}

		// CSRF
		if (configuration.getPages().isCsrfEnabled()) {
			http.csrf().ignoringAntMatchers(configuration.getPages().getPrefix() + "/**");

			// bypass csrf protection on configured pages
			for (String page : configuration.getPages().getCsrfBypass()) {
				http.csrf().ignoringAntMatchers(page);
			}

			// support REST integrations by supplying CRSF token in response header
			http.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("CSRF Disabled");
			}
			http.csrf().disable();
		}

		// setup CSP if one is configured
		if (StringUtils.hasLength(configuration.getSp().getContentSecurityPolicy())) {
			http.headers().contentSecurityPolicy(configuration.getSp().getContentSecurityPolicy());
		}

		// make sure referer header is not send cross-origin
		if (configuration.getPages().isStrictRefererPolicyEnabled()) {
			http.headers().referrerPolicy().policy(ReferrerPolicy.SAME_ORIGIN);
		}

		// Always allow CORS preflight
		// Every request is passed through SAMLFilter which does the rest of the logic
		http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll();
	}
}
