package dk.digitalidentity.samlmodule.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import lombok.extern.slf4j.Slf4j;

// The normal "WebSecurityConfigurerAdapter" Uses Order=100, and it must be unique.
// Applications might extend "WebSecurityConfigurerAdapter" on top of having saml-module installed,
// so we set to 104 to avoid conflicts (but make sure it has higher priority than default SAML security configuration
@Order(104)
@Configuration
@Slf4j
public class SecurityConfigurationXFraming extends WebSecurityConfigurerAdapter {

	@Autowired
	private DISAML_Configuration configuration;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("Configuring XFrameOptions");
		}

		// XFrameOptions can be disabled for all non-secured pages if needed (this is default)
		if (configuration.getPages().isXFrameOptionsDisabledForNonSecuredPages() && configuration.getPages().getNonsecured().size() > 0) {
			for (String page : configuration.getPages().getNonsecured()) {
				http.antMatcher(page)
					.headers().frameOptions().disable().and()
					.csrf().disable();
			}
		}
	}
}
