package dk.digitalidentity.samlmodule.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.opensaml.core.criterion.EntityIdCriterion;
import org.opensaml.security.SecurityException;
import org.opensaml.security.credential.impl.KeyStoreCredentialResolver;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.xmlsec.keyinfo.KeyInfoGenerator;
import org.opensaml.xmlsec.keyinfo.impl.X509KeyInfoGeneratorFactory;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.resolver.CriteriaSet;
import net.shibboleth.utilities.java.support.resolver.ResolverException;

@Slf4j
@Service
public class DISAML_CredentialService {
	private BasicX509Credential basicX509Credential;
	private BasicX509Credential secondaryBasicX509Credential;

	@Autowired
	private DISAML_Configuration configuration;
	
	@Autowired(required = false)
	private DISAML_KeystoreProvider keystoreProvider;
	
	// helper method to ensure a reload
	public void reset() {
		basicX509Credential = null;
		secondaryBasicX509Credential = null;
	}

	public BasicX509Credential getSecondaryBasicX509Credential() throws InternalException {
		if (secondaryBasicX509Credential != null) {
			return secondaryBasicX509Credential;
		}

		// if no keystoreProvider with a secondaryKeystore is available, and no directly configured secondaryKeystore is available, return null
		if ((keystoreProvider == null || keystoreProvider.getSecondaryKeystore() == null) &&
			(!StringUtils.hasLength(configuration.getKeystore().getSecondaryLocation()) || !StringUtils.hasLength(configuration.getKeystore().getSecondaryPassword()))) {
			return null;
		}

		boolean fromProvider = false;
		KeyStore ks = null;
		if (keystoreProvider != null) {
			ks = keystoreProvider.getSecondaryKeystore();
			if (ks != null) {
				fromProvider = true;
			}
		}
		
		if (ks == null) {
			ks = keyStore(configuration.getKeystore().getSecondaryLocation(), configuration.getKeystore().getSecondaryPassword().toCharArray());
		}
		
		Map<String, String> passwords = new HashMap<>();
		String alias = (fromProvider) ? keystoreProvider.getSecondaryKeystoreAlias() : null;
		if (alias != null) {
			passwords.put(alias, (fromProvider) ? keystoreProvider.getSecondaryKeystorePassword() : configuration.getKeystore().getSecondaryPassword());
		}
		else {
			try {
				alias = ks.aliases().nextElement();
				passwords.put(alias, (fromProvider) ? keystoreProvider.getSecondaryKeystorePassword() : configuration.getKeystore().getSecondaryPassword());
			}
			catch (KeyStoreException ex) {
				throw new InternalException("Keystore ikke initialiseret ordentligt", ex);
			}
		}

		KeyStoreCredentialResolver resolver = new KeyStoreCredentialResolver(ks, passwords);

		CriteriaSet criteria = new CriteriaSet();
		EntityIdCriterion entityIdCriterion = new EntityIdCriterion(alias);
		criteria.add(entityIdCriterion);

		try {
			secondaryBasicX509Credential = (BasicX509Credential) resolver.resolveSingle(criteria);

			// Validate that the Certificate is valid now and that it will still be valid in a week
			// This is only done once when the program is loaded since we keep a copy of the credential en memory after the fact
			if (secondaryBasicX509Credential != null && secondaryBasicX509Credential.getEntityCertificate() != null) {
				validateCertificateExpiry();
			}

			return secondaryBasicX509Credential;
		}
		catch (ResolverException e) {
			throw new InternalException("Kunne ikke finde egnet credentials ud fra aliasset: " + alias, e);
		}
	}
	
	public BasicX509Credential getBasicX509Credential() throws InternalException {
		if (basicX509Credential != null) {
			return basicX509Credential;
		}

		boolean fromProvider = false;
		KeyStore ks = null;
		if (keystoreProvider != null) {
			ks = keystoreProvider.getPrimaryKeystore();
			if (ks != null) {
				fromProvider = true;
			}
		}
		
		if (ks == null) {
			ks = keyStore(configuration.getKeystore().getLocation(), configuration.getKeystore().getPassword().toCharArray());
		}
		
		Map<String, String> passwords = new HashMap<>();
		String alias = (fromProvider) ? keystoreProvider.getPrimaryKeystoreAlias() : null;
		if (alias != null) {
			passwords.put(alias, (fromProvider) ? keystoreProvider.getPrimaryKeystorePassword() : configuration.getKeystore().getPassword());
		}
		else {
			try {
				alias = ks.aliases().nextElement();
				passwords.put(alias, (fromProvider) ? keystoreProvider.getPrimaryKeystorePassword() : configuration.getKeystore().getPassword());
			}
			catch (KeyStoreException ex) {
				throw new InternalException("Keystore ikke initialiseret ordentligt", ex);
			}
		}

		KeyStoreCredentialResolver resolver = new KeyStoreCredentialResolver(ks, passwords);

		CriteriaSet criteria = new CriteriaSet();
		EntityIdCriterion entityIdCriterion = new EntityIdCriterion(alias);
		criteria.add(entityIdCriterion);

		try {
			basicX509Credential = (BasicX509Credential) resolver.resolveSingle(criteria);

			// Validate that the Certificate is valid now and that it will still be valid in a week
			// This is only done once when the program is loaded since we keep a copy of the credential en memory after the fact
			if (basicX509Credential != null && basicX509Credential.getEntityCertificate() != null) {
				validateCertificateExpiry();
			}

			return basicX509Credential;
		}
		catch (ResolverException e) {
			throw new InternalException("Kunne ikke finde egnet credentials ud fra aliasset: " + alias, e);
		}
	}

	private void validateCertificateExpiry() {
		try {
			Calendar instance = Calendar.getInstance();
			instance.add(Calendar.DATE, 7);
			Date aWeekFromNow = instance.getTime();
			basicX509Credential.getEntityCertificate().checkValidity(aWeekFromNow);
		}
		catch (CertificateExpiredException | CertificateNotYetValidException ex) {
			log.error("Certificate expiring soon", ex);
		}
		catch (Exception ex) {
			// We do not want this Validity check to stop the code if it goes badly
			log.warn("Failed checking credential expiry", ex);
		}
	}

	public KeyInfo getSecondaryPublicKeyInfo() throws InternalException {
		BasicX509Credential credential = getSecondaryBasicX509Credential();
		if (credential == null) {
			return null;
		}
		
		X509KeyInfoGeneratorFactory x509KeyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();
		x509KeyInfoGeneratorFactory.setEmitEntityCertificate(true);
		KeyInfoGenerator keyInfoGenerator = x509KeyInfoGeneratorFactory.newInstance();

		try {
			return keyInfoGenerator.generate(credential);
		}
		catch (SecurityException e) {
			throw new InternalException("Kunne ikke generere public key ud fra Tjenesteudbyders credentials", e);
		}
	}
	
	public KeyInfo getPublicKeyInfo() throws InternalException {
		X509KeyInfoGeneratorFactory x509KeyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();
		x509KeyInfoGeneratorFactory.setEmitEntityCertificate(true);
		KeyInfoGenerator keyInfoGenerator = x509KeyInfoGeneratorFactory.newInstance();

		try {
			return keyInfoGenerator.generate(getBasicX509Credential());
		}
		catch (SecurityException e) {
			throw new InternalException("Kunne ikke generere public key ud fra Tjenesteudbyders credentials", e);
		}
	}

	private KeyStore keyStore(String file, char[] password) throws InternalException {
		try {
			KeyStore keyStore = KeyStore.getInstance("PKCS12");

			File key = ResourceUtils.getFile(file);

			InputStream in = new FileInputStream(key);
			keyStore.load(in, password);
			return keyStore;
		}
		catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
			throw new InternalException("Kunne ikke tilgå Tjenesteudbyders Keystore", e);
		}
	}
}
