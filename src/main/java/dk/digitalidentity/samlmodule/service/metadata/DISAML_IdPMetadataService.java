package dk.digitalidentity.samlmodule.service.metadata;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.http.client.HttpClient;
import org.bouncycastle.util.encoders.Base64;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.metadata.resolver.impl.AbstractReloadingMetadataResolver;
import org.opensaml.saml.metadata.resolver.impl.FilesystemMetadataResolver;
import org.opensaml.saml.metadata.resolver.impl.HTTPMetadataResolver;
import org.opensaml.saml.metadata.resolver.impl.ResourceBackedMetadataResolver;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml.saml2.metadata.SingleLogoutService;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.security.credential.UsageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.InMemoryResource;
import dk.digitalidentity.samlmodule.model.SamlIdentityProviderProvider;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.resolver.ResolverException;
import net.shibboleth.utilities.java.support.xml.BasicParserPool;

@Slf4j
@Service
public class DISAML_IdPMetadataService {
	private Map<String, AbstractReloadingMetadataResolver> resolvers = new HashMap<>();

	@Autowired
	@Qualifier("DISAML_HTTPClient")
	private HttpClient httpClient;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired(required = false)
	private SamlIdentityProviderProvider identityProviderProvider;
	
	public EntityDescriptor getMetadata(Issuer issuer) throws InternalException, ExternalException {
		IdentityProvider identityProvider = getIdentityProvider(issuer);

		return getMetadata(identityProvider);
	}

	public EntityDescriptor getMetadata(IdentityProvider identityProvider) throws InternalException, ExternalException {
		// if we already have an initialized metadata resolver matching the given entityId, use that one.
		if (resolvers.containsKey(identityProvider.getEntityId())) {
			AbstractReloadingMetadataResolver resolver = resolvers.get(identityProvider.getEntityId());

			if (resolver.isInitialized()) {
				return getMetadata(resolver);
			}
		}

		AbstractReloadingMetadataResolver resolver = getMetadataResolverByEntityId(identityProvider);
		resolvers.put(identityProvider.getEntityId(), resolver);

		return getMetadata(resolver);
	}

	private EntityDescriptor getMetadata(AbstractReloadingMetadataResolver resolver) throws InternalException, ExternalException {
		// if last scheduled refresh failed, refresh now to return an up-to-date metadata
		if (Boolean.FALSE.equals(resolver.wasLastRefreshSuccess())) {
			try {
				resolver.refresh();
			}
			catch (ResolverException ex) {
				log.error("Failed to refresh metadata - continuing with potential stale metadata", ex);
			}
		}

		EntityDescriptor entityDescriptor = resolver.iterator().next();
		if (entityDescriptor == null) {
			log.warn("Could not resolve metadata for: " + configuration.getIdp().getMetadataLocation());
		}

		return entityDescriptor;
	}

	public IDPSSODescriptor getSSODescriptor(IdentityProvider identityProvider) throws InternalException, ExternalException {
		EntityDescriptor entityDescriptor = getMetadata(identityProvider);
		if (entityDescriptor == null) {
			throw new ExternalException("Could not load IdP metadata");
		}
		
		return entityDescriptor.getIDPSSODescriptor(SAMLConstants.SAML20P_NS);
	}

	public List<PublicKey> getPublicKeys(IdentityProvider identityProvider, UsageType usageType) throws InternalException, ExternalException {
		return getX509Certificates(identityProvider, usageType).stream().map(c -> c.getPublicKey()).collect(Collectors.toList());
	}

	public List<X509Certificate> getX509Certificates(IdentityProvider identityProvider, UsageType usageType) throws InternalException, ExternalException {
		List<X509Certificate> certificates = new ArrayList<>();

		// Find X509Cert in Metadata filtered by type
		List<KeyDescriptor> keyDescriptors = getSSODescriptor(identityProvider).getKeyDescriptors().stream()
				.filter(keyDescriptor -> keyDescriptor.getUse().equals(usageType))
				.collect(Collectors.toList());

		for (KeyDescriptor keyDescriptor : keyDescriptors) {
			try {
				org.opensaml.xmlsec.signature.X509Certificate x509Certificate = keyDescriptor.getKeyInfo().getX509Datas().get(0).getX509Certificates().get(0);
		
				byte[] bytes = Base64.decode(x509Certificate.getValue());
				ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
				CertificateFactory instance = CertificateFactory.getInstance("X.509");
	
				certificates.add((X509Certificate) instance.generateCertificate(inputStream));
			}
			catch (Exception ex) {
				log.error("Could not parse certificate from IdP metadata - skipping", ex);
			}
		}
		
		return certificates;
	}

	public SingleSignOnService getAuthnRequestEndpoint(IdentityProvider identityProvider) throws InternalException, ExternalException {
		Optional<SingleSignOnService> match = getSSODescriptor(identityProvider).getSingleSignOnServices().stream()
				.filter(singleSignOnService -> singleSignOnService.getBinding().equals(SAMLConstants.SAML2_REDIRECT_BINDING_URI)).findFirst();

		if (match.isEmpty()) {
			throw new ExternalException("Could not find SSO endpoint for Redirect binding in metadata");
		}

		return match.get();
	}

	public SingleLogoutService getLogoutEndpoint(IdentityProvider identityProvider) throws InternalException, ExternalException {
		Optional<SingleLogoutService> match = getSSODescriptor(identityProvider).getSingleLogoutServices().stream()
				.filter(singleLogoutService -> singleLogoutService.getBinding().equals(SAMLConstants.SAML2_REDIRECT_BINDING_URI)).findFirst();

		if (match.isEmpty()) {
			throw new ExternalException("Could not find SLO endpoint for Redirect binding in metadata");
		}

		return match.get();
	}

	public String getLogoutResponseEndpoint(IdentityProvider identityProvider) throws ExternalException, InternalException {
		Optional<SingleLogoutService> match = getSSODescriptor(identityProvider).getSingleLogoutServices()
				.stream()
				.filter(sloService -> SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(sloService.getBinding()) && StringUtils.hasLength(sloService.getResponseLocation())
				)
				.findFirst();

		if (match.isPresent()) {
			return match.get().getResponseLocation();
		}
		else {
			// if no response location is found return to logout endpoint
			log.warn("Unable to find SingleLogoutService with binding HTTPRedirect");
			
			//TODO Check: is this ok? i don't think its following the spec to the letter but i'm pretty sure this is expected behaviour
			return getLogoutEndpoint(identityProvider).getLocation();
		}
	}

	public IdentityProvider getIdentityProvider(@Nullable Issuer issuer) throws InternalException {
		if (configuration.getIdp().isDiscovery()) {
			// Configuration checks
			if (identityProviderProvider == null) {
				throw new InternalException("Discovery was enabled but no instance of SamlIdentityProviderProvider was implemented");
			}

			List<IdentityProvider> identityProviders = identityProviderProvider.getIdentityProviders();
			if (identityProviders == null || identityProviders.isEmpty()) {
				throw new InternalException("Discovery was enabled but no Identity Providers was configured");
			}

			if (issuer == null || !StringUtils.hasLength(issuer.getValue())) {
				throw new InternalException("Issuer was null or had an empty value, could not determine IdentityProvider");
			}

			String entityId = issuer.getValue();

			// Find the provider
			IdentityProvider identityProvider = identityProviderProvider.getByEntityId(entityId);
			if (identityProvider == null) {
				throw new InternalException("Discovery was enabled but no Identity Provider matching the given entityId was configured: " + entityId);
			}

			return identityProvider;
		}
		else {
			// Return IdentityProvider matching the configured one with a placeholder entityId
			return new IdentityProvider(configuration);
		}
	}

	public IdentityProvider getIdentityProvider(String entityId) throws InternalException, ExternalException {
		if (configuration.getIdp().isDiscovery()) {
			// Configuration checks
			if (identityProviderProvider == null) {
				throw new InternalException("Discovery was enabled but no instance of SamlIdentityProviderProvider was implemented");
			}

			List<IdentityProvider> identityProviders = identityProviderProvider.getIdentityProviders();
			if (identityProviders == null || identityProviders.isEmpty()) {
				throw new InternalException("Discovery was enabled but no Identity Providers was configured");
			}

			// Find the provider
			IdentityProvider identityProvider = identityProviderProvider.getByEntityId(entityId);
			if (identityProvider == null) {
				throw new ExternalException("Discovery was enabled but no Identity Provider matching the given entityId was configured: " + entityId);
			}

			return identityProvider;
		}
		else {
			// Return IdentityProvider matching the configured one with a placeholder entityId
			return new IdentityProvider(configuration);
		}
	}

	private AbstractReloadingMetadataResolver getMetadataResolverByEntityId(IdentityProvider identityProvider) throws InternalException {
		// initialize the appropriate type of metadata resolver
		if (identityProvider.getMetadata().startsWith("file:")) {
			return createFileMetadataResolver(identityProvider.getMetadata().substring("file:".length()));
		}
		else if (identityProvider.getMetadata().startsWith("url:")) {
			return createURLMetadataResolver(identityProvider.getMetadata().substring("url:".length()));
		}
		else if (identityProvider.getMetadata().startsWith("http://") || identityProvider.getMetadata().startsWith("https://")) {
			return createURLMetadataResolver(identityProvider.getMetadata());
		}
		else {
			// If it is not using either file or url prefixes try to just parse it as a string representing the metadata xml
			return createInMemoryMetadataResolver(identityProvider.getMetadata());
		}
	}

	private AbstractReloadingMetadataResolver createFileMetadataResolver(String location) throws InternalException {
		FilesystemMetadataResolver resolver;
		try {
			resolver = new FilesystemMetadataResolver(new File(location));
			resolver.setId(UUID.randomUUID().toString());
			resolver.setMinRefreshDelay(1000 * 60 * 60 * configuration.getIdp().getResolverMinRefreshDelay());
			resolver.setMaxRefreshDelay(1000 * 60 * 60 * configuration.getIdp().getResolverMaxRefreshDelay());
		}
		catch (ResolverException e) {
			throw new InternalException("Could not create MetadataResolver", e);
		}

		return parseAndInitializeMetadataResolver(resolver);
	}

	private AbstractReloadingMetadataResolver createURLMetadataResolver(String metadataURL) throws InternalException {
		// if no resolver exists for this ServiceProvider, create it.
		HTTPMetadataResolver resolver;
		try {
			resolver = new HTTPMetadataResolver(httpClient, metadataURL);
			resolver.setId(UUID.randomUUID().toString());
			resolver.setMinRefreshDelay(1000 * 60 * 60 * configuration.getIdp().getResolverMinRefreshDelay());
			resolver.setMaxRefreshDelay(1000 * 60 * 60 * configuration.getIdp().getResolverMaxRefreshDelay());
		}
		catch (ResolverException e) {
			throw new InternalException("Could not create MetadataResolver", e);
		}

		return parseAndInitializeMetadataResolver(resolver);
	}

	private AbstractReloadingMetadataResolver createInMemoryMetadataResolver(String metadata) throws InternalException {
		ResourceBackedMetadataResolver resolver;
		try {
			resolver = new ResourceBackedMetadataResolver(new InMemoryResource(metadata.getBytes(StandardCharsets.UTF_8)));
			resolver.setId(UUID.randomUUID().toString());
		}
		catch (IOException e) {
			throw new InternalException("Could not create MetadataResolver", e);
		}

		return parseAndInitializeMetadataResolver(resolver);
	}

	private AbstractReloadingMetadataResolver parseAndInitializeMetadataResolver(AbstractReloadingMetadataResolver resolver) throws InternalException {
		if (resolver.isInitialized()) {
			return resolver;
		}

		// create parser pool for parsing metadata
		BasicParserPool parserPool = new BasicParserPool();
		resolver.setParserPool(parserPool);

		try {
			parserPool.initialize();
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize parser pool", e);
		}

		// initialize and save resolver for future use
		try {
			resolver.initialize();
			return resolver;
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize MetadataResolver", e);
		}
	}
}