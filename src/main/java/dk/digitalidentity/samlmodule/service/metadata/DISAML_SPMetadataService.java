package dk.digitalidentity.samlmodule.service.metadata;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.Attribute;
import org.opensaml.saml.saml2.core.NameIDType;
import org.opensaml.saml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml.saml2.metadata.AttributeConsumingService;
import org.opensaml.saml.saml2.metadata.ContactPerson;
import org.opensaml.saml.saml2.metadata.ContactPersonTypeEnumeration;
import org.opensaml.saml.saml2.metadata.EmailAddress;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml.saml2.metadata.NameIDFormat;
import org.opensaml.saml.saml2.metadata.RequestedAttribute;
import org.opensaml.saml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml.saml2.metadata.SSODescriptor;
import org.opensaml.saml.saml2.metadata.ServiceName;
import org.opensaml.saml.saml2.metadata.SingleLogoutService;
import org.opensaml.security.credential.UsageType;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.service.DISAML_CredentialService;
import dk.digitalidentity.samlmodule.service.saml.DISAML_OpenSAMLHelperService;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;

@Service
public class DISAML_SPMetadataService {

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_OpenSAMLHelperService samlHelper;

	@Autowired
	private DISAML_CredentialService credentialService;

	public EntityDescriptor getMetadata() throws InternalException {
		EntityDescriptor entityDescriptor = createEntityDescriptor();

		// Create IdPSSODescriptor
		SPSSODescriptor spssoDescriptor = samlHelper.buildSAMLObject(SPSSODescriptor.class);
		entityDescriptor.getRoleDescriptors().add(spssoDescriptor);

		spssoDescriptor.addSupportedProtocol(SAMLConstants.SAML20P_NS);
		spssoDescriptor.setAuthnRequestsSigned(true);
		spssoDescriptor.setWantAssertionsSigned(true);


		// Create NameIDFormat
		NameIDFormat nameIDFormat = samlHelper.buildSAMLObject(NameIDFormat.class);
		spssoDescriptor.getNameIDFormats().add(nameIDFormat);

		nameIDFormat.setFormat(NameIDType.PERSISTENT);


		// Create ContactPerson with technical contact email
		ContactPerson contactPerson = samlHelper.buildSAMLObject(ContactPerson.class);
		spssoDescriptor.getContactPersons().add(contactPerson);

		contactPerson.setType(ContactPersonTypeEnumeration.TECHNICAL);

		EmailAddress emailAddress = samlHelper.buildSAMLObject(EmailAddress.class);
		contactPerson.getEmailAddresses().add(emailAddress);

		emailAddress.setAddress(configuration.getSp().getTechnicalContactEmail());


		// Encryption and Signing descriptors
		List<KeyDescriptor> keyDescriptors = spssoDescriptor.getKeyDescriptors();

		// add both primary and secondary signing cert to metadata
		keyDescriptors.add(getKeyDescriptor(UsageType.SIGNING));
		KeyDescriptor secondaryKeyDescriptor = getSecondaryKeyDescriptor(UsageType.SIGNING);
		if (secondaryKeyDescriptor != null) {
			keyDescriptors.add(secondaryKeyDescriptor);			
		}

		// add the 2nd certificate for encryption if one is available - otherwise pick the primary
		// this allows us a more fluent rollover from primary to secondary
		KeyDescriptor secondaryEncryptionKeyDescriptor = getSecondaryKeyDescriptor(UsageType.ENCRYPTION);
		if (secondaryEncryptionKeyDescriptor != null) {
			keyDescriptors.add(secondaryEncryptionKeyDescriptor);			
		}
		else {
			keyDescriptors.add(getKeyDescriptor(UsageType.ENCRYPTION));
		}

		// Create AssertionConsumerService endpoint
		AssertionConsumerService assertionConsumerService = samlHelper.buildSAMLObject(AssertionConsumerService.class);
		spssoDescriptor.getAssertionConsumerServices().add(assertionConsumerService);

		assertionConsumerService.setBinding(SAMLConstants.SAML2_POST_BINDING_URI);
		assertionConsumerService.setLocation(getAssertionConsumerService());
		assertionConsumerService.setIsDefault(true);
		assertionConsumerService.setIndex(0);

		// Create SLO Redirect endpoint
		SingleLogoutService logoutRedirect = samlHelper.buildSAMLObject(SingleLogoutService.class);
		spssoDescriptor.getSingleLogoutServices().add(logoutRedirect);

		logoutRedirect.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
		logoutRedirect.setLocation(getLogoutEndpoint());
		logoutRedirect.setResponseLocation(getLogoutResponseEndpoint());

		// Create SLO Post endpoint
		SingleLogoutService logoutPost = samlHelper.buildSAMLObject(SingleLogoutService.class);
		spssoDescriptor.getSingleLogoutServices().add(logoutPost);

		logoutPost.setBinding(SAMLConstants.SAML2_POST_BINDING_URI);
		logoutPost.setLocation(getLogoutEndpoint());
		logoutPost.setResponseLocation(getLogoutResponseEndpoint());


		// Create required OIO Attributes
		AttributeConsumingService attributeConsumingService = samlHelper.buildSAMLObject(AttributeConsumingService.class);
		spssoDescriptor.getAttributeConsumingServices().add(attributeConsumingService);

		attributeConsumingService.setIsDefault(true);

		ServiceName serviceName = samlHelper.buildSAMLObject(ServiceName.class);
		serviceName.setXMLLang("da-DK");
		attributeConsumingService.getNames().add(serviceName);

		serviceName.setValue(configuration.getSp().getEntityId());

		List<RequestedAttribute> requestAttributes = attributeConsumingService.getRequestAttributes();
		createRequestedAttribute(requestAttributes, "https://data.gov.dk/model/core/specVersion", "SpecVer", true);
		createRequestedAttribute(requestAttributes, "https://data.gov.dk/concept/core/nsis/loa", "Level of Assurance", true);
		createRequestedAttribute(requestAttributes, "https://data.gov.dk/model/core/eid/cprNumber", "cpr", true);
		createRequestedAttribute(requestAttributes, "https://data.gov.dk/model/core/eid/professional/cvr", "CVRnumberIdentifier", false);
		createRequestedAttribute(requestAttributes, "https://data.gov.dk/model/core/eid/professional/orgName", "organizationName", false);

		return entityDescriptor;
	}

	public String getLogoutEndpoint() {
		return getUrl() + configuration.getPages().getLogoutMetadataPath();
	}

	public String getLogoutResponseEndpoint() {
		return getUrl() + configuration.getPages().getLogoutMetadataPath() + "/response";
	}

	public String getAssertionConsumerService() {
		return getUrl() + configuration.getPages().getLoginMetadataPath();
	}

	public SPSSODescriptor getSSODescriptor() throws InternalException {
		return getMetadata().getSPSSODescriptor(SAMLConstants.SAML20P_NS);
	}

	private void createRequestedAttribute(List<RequestedAttribute> requestAttributes, String name, String friendlyName, boolean isRequired) {
		RequestedAttribute requestedAttribute = samlHelper.buildSAMLObject(RequestedAttribute.class);
		requestedAttribute.setIsRequired(isRequired);
		requestedAttribute.setName(name);
		requestedAttribute.setFriendlyName(friendlyName);
		requestedAttribute.setNameFormat(Attribute.URI_REFERENCE);
		requestAttributes.add(requestedAttribute);
	}

	private EntityDescriptor createEntityDescriptor() {
		EntityDescriptor entityDescriptor = samlHelper.buildSAMLObject(EntityDescriptor.class);
		entityDescriptor.setEntityID(configuration.getSp().getEntityId());
		entityDescriptor.setID("_" + UUID.nameUUIDFromBytes(configuration.getSp().getEntityId().getBytes()).toString());

		return entityDescriptor;
	}

	private KeyDescriptor getSecondaryKeyDescriptor(UsageType usageType) throws InternalException {
		KeyInfo keyInfo = credentialService.getSecondaryPublicKeyInfo();
		if (keyInfo == null) {
			return null;
		}
		
		KeyDescriptor keyDescriptor = samlHelper.buildSAMLObject(KeyDescriptor.class);

		keyDescriptor.setUse(usageType);
		keyDescriptor.setKeyInfo(keyInfo);

		return keyDescriptor;
	}

	private KeyDescriptor getKeyDescriptor(UsageType usageType) throws InternalException {
		KeyDescriptor keyDescriptor = samlHelper.buildSAMLObject(KeyDescriptor.class);

		keyDescriptor.setUse(usageType);
		keyDescriptor.setKeyInfo(credentialService.getPublicKeyInfo());

		return keyDescriptor;
	}

	private String getUrl() {
		String baseUrl = configuration.getSp().getBaseUrl();
		if (baseUrl.endsWith("/")) {
			baseUrl = baseUrl.substring(0, baseUrl.length()-1);
		}

		return baseUrl + configuration.getPages().getPrefix();
	}

	public SingleLogoutService getLogoutEndpoint(SSODescriptor ssoDescriptor) throws InternalException {
		Optional<SingleLogoutService> match = ssoDescriptor.getSingleLogoutServices().stream()
				.filter(singleSignOnService -> singleSignOnService.getBinding().equals(SAMLConstants.SAML2_REDIRECT_BINDING_URI)).findFirst();

		if (match.isEmpty()) {
			throw new InternalException("Could not find SLO endpoint for Redirect binding in metadata");
		}

		return match.get();
	}
}