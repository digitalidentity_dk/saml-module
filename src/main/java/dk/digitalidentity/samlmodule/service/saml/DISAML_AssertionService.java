package dk.digitalidentity.samlmodule.service.saml;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.opensaml.messaging.context.MessageContext;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.EncryptedAssertion;
import org.opensaml.saml.saml2.core.Response;
import org.opensaml.saml.saml2.encryption.Decrypter;
import org.opensaml.saml.saml2.encryption.EncryptedElementTypeEncryptedKeyResolver;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.xmlsec.encryption.support.ChainingEncryptedKeyResolver;
import org.opensaml.xmlsec.encryption.support.DecryptionException;
import org.opensaml.xmlsec.encryption.support.EncryptedKeyResolver;
import org.opensaml.xmlsec.encryption.support.InlineEncryptedKeyResolver;
import org.opensaml.xmlsec.encryption.support.SimpleRetrievalMethodEncryptedKeyResolver;
import org.opensaml.xmlsec.keyinfo.KeyInfoCredentialResolver;
import org.opensaml.xmlsec.keyinfo.impl.StaticKeyInfoCredentialResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.samlmodule.service.DISAML_CredentialService;
import dk.digitalidentity.samlmodule.util.RequestDecodeUtil;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DISAML_AssertionService {

	@Autowired
	private DISAML_CredentialService credentialService;

	public MessageContext<SAMLObject> getMessageContext(HttpServletRequest request) throws InternalException, ExternalException {
		return RequestDecodeUtil.getMessageContext(request);
	}

	public Response getResponse(MessageContext<SAMLObject> messageContext) {
		return (Response) messageContext.getMessage();
	}

	public Assertion getAssertion(Response response) throws InternalException, ExternalException {
		if (!response.getEncryptedAssertions().isEmpty()) {
			EncryptedAssertion encryptedAssertion = response.getEncryptedAssertions().get(0);

			return decryptAssertion(encryptedAssertion);
		}
		else if (!response.getAssertions().isEmpty()) {
			return response.getAssertions().get(0);
		}

		throw new ExternalException("No assertion in SAML response!");
	}

	private Assertion decryptAssertion(EncryptedAssertion encryptedAssertion) throws InternalException, ExternalException {
		try {
			BasicX509Credential credentials = credentialService.getBasicX509Credential();

			return decryptAssertionUsingCredentials(encryptedAssertion, credentials);
		}
		catch (DecryptionException e) {
			BasicX509Credential secondaryCredentials = credentialService.getSecondaryBasicX509Credential();
			if (secondaryCredentials != null) {
				try {
					log.warn("Failed to decrypt using primary credentials, trying secondary credentials");
					
					return decryptAssertionUsingCredentials(encryptedAssertion, secondaryCredentials);
				}
				catch (DecryptionException ex) {
					throw new ExternalException("Could not decrypt provided EncryptedAssertion using secondary credentials", ex);
				}
			}

			throw new ExternalException("Could not decrypt provided EncryptedAssertion", e);
		}
	}

	private Assertion decryptAssertionUsingCredentials(EncryptedAssertion encryptedAssertion, BasicX509Credential credentials) throws DecryptionException {
		KeyInfoCredentialResolver keyResolver = new StaticKeyInfoCredentialResolver(credentials);

		List<EncryptedKeyResolver> encryptedKeyResolvers = new ArrayList<>();
		encryptedKeyResolvers.add(new InlineEncryptedKeyResolver());
		encryptedKeyResolvers.add(new EncryptedElementTypeEncryptedKeyResolver());
		encryptedKeyResolvers.add(new SimpleRetrievalMethodEncryptedKeyResolver());

		ChainingEncryptedKeyResolver kekResolver = new ChainingEncryptedKeyResolver(encryptedKeyResolvers);

		Decrypter decrypter = new Decrypter(null, keyResolver, kekResolver);
		decrypter.setRootInNewDocument(true);
	
		return decrypter.decrypt(encryptedAssertion);
	}
}
