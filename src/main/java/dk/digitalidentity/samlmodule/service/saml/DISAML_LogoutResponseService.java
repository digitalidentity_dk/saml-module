package dk.digitalidentity.samlmodule.service.saml;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.messaging.context.SAMLEndpointContext;
import org.opensaml.saml.common.messaging.context.SAMLPeerEntityContext;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.LogoutRequest;
import org.opensaml.saml.saml2.core.LogoutResponse;
import org.opensaml.saml.saml2.core.Status;
import org.opensaml.saml.saml2.core.StatusCode;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.xmlsec.SignatureSigningParameters;
import org.opensaml.xmlsec.context.SecurityParametersContext;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.service.DISAML_CredentialService;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.service.validation.DISAML_LogoutResponseValidationService;
import dk.digitalidentity.samlmodule.util.RequestDecodeUtil;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import net.shibboleth.utilities.java.support.security.RandomIdentifierGenerationStrategy;

@Service
public class DISAML_LogoutResponseService {

	@Autowired
	private DISAML_LogoutResponseValidationService validationService;

	@Autowired
	private DISAML_OpenSAMLHelperService samlHelperService;

	@Autowired
	private DISAML_CredentialService credentialService;

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@Autowired
	private DISAML_Configuration configuration;

	public MessageContext<SAMLObject> getMessageContext(HttpServletRequest request) throws InternalException, ExternalException {
		return RequestDecodeUtil.getMessageContext(request);
	}

	public LogoutResponse getLogoutResponse(MessageContext<SAMLObject> messageContext) {
		return (LogoutResponse) messageContext.getMessage();
	}

	public void validateLogoutResponse(HttpServletRequest request, MessageContext<SAMLObject> messageContext, LogoutRequest logoutRequest, IdentityProvider identityProvider) throws InternalException, ExternalException {
		validationService.validate(request, messageContext, logoutRequest, identityProvider);
	}

	public MessageContext<SAMLObject> createLogoutResponse(LogoutRequest logoutRequest) throws ExternalException, InternalException {
		// Create message context
		MessageContext<SAMLObject> messageContext = new MessageContext<>();

		// Determine IdP
		IdentityProvider identityProvider = idPMetadataService.getIdentityProvider(logoutRequest.getIssuer());

		// Create LogoutResponse
		String destination = idPMetadataService.getLogoutResponseEndpoint(identityProvider);
		LogoutResponse logoutResponse = createLogoutReponseObj(logoutRequest, destination);
		messageContext.setMessage(logoutResponse);

		// Destination
		SAMLPeerEntityContext peerEntityContext = messageContext.getSubcontext(SAMLPeerEntityContext.class, true);
		SAMLEndpointContext endpointContext = peerEntityContext.getSubcontext(SAMLEndpointContext.class, true);

		SingleSignOnService endpoint = samlHelperService.buildSAMLObject(SingleSignOnService.class);
		endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
		endpoint.setLocation(destination);

		endpointContext.setEndpoint(endpoint);

		// Signing info
		SignatureSigningParameters signatureSigningParameters = new SignatureSigningParameters();
		signatureSigningParameters.setSigningCredential(credentialService.getBasicX509Credential());
		signatureSigningParameters.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);
		messageContext.getSubcontext(SecurityParametersContext.class, true).setSignatureSigningParameters(signatureSigningParameters);

		return messageContext;
	}

	public LogoutResponse createLogoutReponseObj(LogoutRequest logoutRequest, String destination) {
		LogoutResponse logoutResponse = samlHelperService.buildSAMLObject(LogoutResponse.class);

		logoutResponse.setID(new RandomIdentifierGenerationStrategy().generateIdentifier());
		logoutResponse.setDestination(destination);
		logoutResponse.setIssueInstant(new DateTime());
		logoutResponse.setInResponseTo(logoutRequest.getID());

		// Create Issuer
		Issuer issuer = samlHelperService.buildSAMLObject(Issuer.class);
		logoutResponse.setIssuer(issuer);

		issuer.setValue(configuration.getSp().getEntityId());

		// Create Status
		Status status = samlHelperService.buildSAMLObject(Status.class);
		logoutResponse.setStatus(status);

		StatusCode statusCode = samlHelperService.buildSAMLObject(StatusCode.class);
		status.setStatusCode(statusCode);

		statusCode.setValue(StatusCode.SUCCESS);

		return logoutResponse;
	}
}
