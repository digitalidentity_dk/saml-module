package dk.digitalidentity.samlmodule.service.saml;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.messaging.context.SAMLEndpointContext;
import org.opensaml.saml.common.messaging.context.SAMLPeerEntityContext;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.LogoutRequest;
import org.opensaml.saml.saml2.core.NameID;
import org.opensaml.saml.saml2.core.SessionIndex;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.xmlsec.SignatureSigningParameters;
import org.opensaml.xmlsec.context.SecurityParametersContext;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.TokenUser;
import dk.digitalidentity.samlmodule.service.DISAML_CredentialService;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.service.validation.DISAML_LogoutRequestValidationService;
import dk.digitalidentity.samlmodule.util.RequestDecodeUtil;
import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import net.shibboleth.utilities.java.support.security.RandomIdentifierGenerationStrategy;

@Service
public class DISAML_LogoutRequestService {

	@Autowired
	private DISAML_LogoutRequestValidationService validationService;

	@Autowired
	private DISAML_OpenSAMLHelperService samlHelperService;

	@Autowired
	private DISAML_CredentialService credentialService;

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	public MessageContext<SAMLObject> getMessageContext(HttpServletRequest request) throws InternalException, ExternalException {
		return RequestDecodeUtil.getMessageContext(request);
	}

	public LogoutRequest getLogoutRequest(MessageContext<SAMLObject> messageContext) {
		return (LogoutRequest) messageContext.getMessage();
	}

	public void validateLogoutRequest(HttpServletRequest request, MessageContext<SAMLObject> messageContext, IdentityProvider identityProvider) throws InternalException, ExternalException {
		validationService.validate(request, messageContext, identityProvider);
	}

	public MessageContext<SAMLObject> createLogoutRequest(IdentityProvider identityProvider) throws InternalException, ExternalException {
		// Create message context
		MessageContext<SAMLObject> messageContext = new MessageContext<>();

		String destination = idPMetadataService.getLogoutEndpoint(identityProvider).getLocation();

		// Create AuthnRequest
		LogoutRequest proxyLogoutRequest = createLogoutRequestObj(destination);
		messageContext.setMessage(proxyLogoutRequest);

		// Destination
		SAMLPeerEntityContext peerEntityContext = messageContext.getSubcontext(SAMLPeerEntityContext.class, true);
		SAMLEndpointContext endpointContext = peerEntityContext.getSubcontext(SAMLEndpointContext.class, true);

		SingleSignOnService endpoint = samlHelperService.buildSAMLObject(SingleSignOnService.class);
		endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
		endpoint.setLocation(destination);

		endpointContext.setEndpoint(endpoint);

		// Signing info
		SignatureSigningParameters signatureSigningParameters = new SignatureSigningParameters();
		signatureSigningParameters.setSigningCredential(credentialService.getBasicX509Credential());
		signatureSigningParameters.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);
		messageContext.getSubcontext(SecurityParametersContext.class, true).setSignatureSigningParameters(signatureSigningParameters);

		return messageContext;
	}

	public LogoutRequest createLogoutRequestObj(String destination) throws InternalException {
		LogoutRequest logoutRequest = samlHelperService.buildSAMLObject(LogoutRequest.class);

		logoutRequest.setID(new RandomIdentifierGenerationStrategy().generateIdentifier());
		logoutRequest.setDestination(destination);
		logoutRequest.setIssueInstant(new DateTime());

		// Set reason
		logoutRequest.setReason(LogoutRequest.USER_REASON);

		// Create Issuer
		Issuer issuer = samlHelperService.buildSAMLObject(Issuer.class);
		logoutRequest.setIssuer(issuer);

		issuer.setValue(configuration.getSp().getEntityId());

		// Copy NameID
		NameID nameId = samlHelperService.buildSAMLObject(NameID.class);
		logoutRequest.setNameID(nameId);

		nameId.setFormat(NameID.X509_SUBJECT);
		if (SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof TokenUser) {
			nameId.setValue(((TokenUser)SecurityContextHolder.getContext().getAuthentication().getDetails()).getUsername());
		}
		else {
			nameId.setValue(SecurityContextHolder.getContext().getAuthentication().getName());
		}

		String sessionIndexID = sessionHelper.getString(SessionConstant.SESSION_INDEX);
		if (sessionIndexID != null) {
			// Fetch session index from session
			SessionIndex sessionIndex = samlHelperService.buildSAMLObject(SessionIndex.class);
			logoutRequest.getSessionIndexes().add(sessionIndex);

			sessionIndex.setSessionIndex(sessionIndexID);
		}

		return logoutRequest;
	}
}
