package dk.digitalidentity.samlmodule.service.saml;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.decoder.MessageDecodingException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.messaging.context.SAMLEndpointContext;
import org.opensaml.saml.common.messaging.context.SAMLPeerEntityContext;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.binding.decoding.impl.HTTPRedirectDeflateDecoder;
import org.opensaml.saml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml.saml2.core.AuthnContextComparisonTypeEnumeration;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.RequestedAuthnContext;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.xmlsec.SignatureSigningParameters;
import org.opensaml.xmlsec.context.SecurityParametersContext;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.enums.NSISLevel;
import dk.digitalidentity.samlmodule.service.DISAML_CredentialService;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_SPMetadataService;
import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.SessionException;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.security.RandomIdentifierGenerationStrategy;
import net.shibboleth.utilities.java.support.xml.BasicParserPool;

@Service
public class DISAML_AuthnRequestService {

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@Autowired
	private DISAML_SPMetadataService sPMetadataService;

	@Autowired
	private DISAML_OpenSAMLHelperService samlHelperService;

	@Autowired
	private DISAML_CredentialService credentialService;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	public MessageContext<SAMLObject> getMessageContext(HttpServletRequest request) throws InternalException, ExternalException {
		try {
			HTTPRedirectDeflateDecoder decoder = new HTTPRedirectDeflateDecoder();
			decoder.setHttpServletRequest(request);

			BasicParserPool parserPool = new BasicParserPool();
			parserPool.initialize();

			decoder.setParserPool(parserPool);
			decoder.initialize();
			decoder.decode();

			MessageContext<SAMLObject> msgContext = decoder.getMessageContext();
			decoder.destroy();

			return msgContext;
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize decoder", e);
		}
		catch (MessageDecodingException e) {
			throw new ExternalException("Could not decode request", e);
		}
	}

	public AuthnRequest getAuthnRequest(MessageContext<SAMLObject> messageContext) {
		return (AuthnRequest) messageContext.getMessage();
	}

	public MessageContext<SAMLObject> createAuthnRequest(IdentityProvider identityProvider) throws InternalException, ExternalException {
		// Create message context
		MessageContext<SAMLObject> messageContext = new MessageContext<>();

		// Get Destination URL from IdP metadata
		String destination = idPMetadataService.getAuthnRequestEndpoint(identityProvider).getLocation();

		// Create AuthnRequest
		AuthnRequest newAuthnRequest = createAuthnRequestObject(destination, identityProvider);
		messageContext.setMessage(newAuthnRequest);

		// Destination
		SAMLPeerEntityContext peerEntityContext = messageContext.getSubcontext(SAMLPeerEntityContext.class, true);
		SAMLEndpointContext endpointContext = peerEntityContext.getSubcontext(SAMLEndpointContext.class, true);

		SingleSignOnService endpoint = samlHelperService.buildSAMLObject(SingleSignOnService.class);
		endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
		endpoint.setLocation(destination);

		endpointContext.setEndpoint(endpoint);

		// Signing info
		SignatureSigningParameters signatureSigningParameters = new SignatureSigningParameters();
		signatureSigningParameters.setSigningCredential(credentialService.getBasicX509Credential());
		signatureSigningParameters.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);
		messageContext.getSubcontext(SecurityParametersContext.class, true).setSignatureSigningParameters(signatureSigningParameters);

		return messageContext;
	}

	private AuthnRequest createAuthnRequestObject(String destination, IdentityProvider identityProvider) throws SessionException {
		//Create new AuthnRequest
		AuthnRequest authnRequest = samlHelperService.buildSAMLObject(AuthnRequest.class);

		// Set AssertionConsumerURL
		authnRequest.setID(new RandomIdentifierGenerationStrategy().generateIdentifier());
		authnRequest.setDestination(destination);
		authnRequest.setIssueInstant(new DateTime());
		authnRequest.setIsPassive(configuration.getSp().isPassive());
		authnRequest.setForceAuthn(configuration.getSp().isForceAuthn());
		authnRequest.setProtocolBinding(SAMLConstants.SAML2_POST_BINDING_URI);
		authnRequest.setAssertionConsumerServiceURL(sPMetadataService.getAssertionConsumerService());

		// Set Issuer
		Issuer issuer = samlHelperService.buildSAMLObject(Issuer.class);
		authnRequest.setIssuer(issuer);

		issuer.setValue(configuration.getSp().getEntityId());

		// AuthnContextClassRef
		if (identityProvider.isContextClassRefEnabled()) {
			boolean elementsPresent = false;
			RequestedAuthnContext requestedAuthnContext = samlHelperService.buildSAMLObject(RequestedAuthnContext.class);
			requestedAuthnContext.setComparison(AuthnContextComparisonTypeEnumeration.MINIMUM);

			// If a needed NSIS Level (LOA) has been specified on the session (by the SAMLFilter) then we include it in the AuthnRequest
			String nsisLevelString = sessionHelper.getString(SessionConstant.NEEDED_NSIS_LEVEL);
			if (nsisLevelString != null) {
				NSISLevel nsisLevel = NSISLevel.fromClaimValue(nsisLevelString);
				switch (nsisLevel) {
					case HIGH:
					case LOW:
					case SUBSTANTIAL:
						AuthnContextClassRef loaRequirement = samlHelperService.buildSAMLObject(AuthnContextClassRef.class);
						requestedAuthnContext.getAuthnContextClassRefs().add(loaRequirement);

						loaRequirement.setAuthnContextClassRef("https://data.gov.dk/concept/core/nsis/loa/" + nsisLevelString);
						elementsPresent = true;
						break;
					case NONE:
						break;
				}
			}

			// Needs brokering
			if (sessionHelper.getBoolean(SessionConstant.BROKERING_ENABLED)) {
				AuthnContextClassRef brokeringRequirement = samlHelperService.buildSAMLObject(AuthnContextClassRef.class);
				requestedAuthnContext.getAuthnContextClassRefs().add(brokeringRequirement);

				brokeringRequirement.setAuthnContextClassRef("https://saml.digital-identity.dk/enable_brokering");
				elementsPresent = true;
			}
			
			boolean requirePersonalProfile = false;
			boolean requireProfessionalProfile = false;
			
			// Profile requirement if specified by the identityProvider configuration
			if (identityProvider.isRequirePersonProfile() || (sessionHelper.getBoolean(SessionConstant.PERSONAL_PROFILE_ENABLED) && configuration.getIdp().isSelectProfilesEnabled())) {
				requirePersonalProfile = true;
			}

			if (sessionHelper.getBoolean(SessionConstant.PROFESSIONAL_PROFILE_ENABLED) && configuration.getIdp().isSelectProfilesEnabled()) {
				requireProfessionalProfile = true;
			}
			
			sessionHelper.removeAttribute(SessionConstant.PERSONAL_PROFILE_ENABLED);
			sessionHelper.removeAttribute(SessionConstant.PROFESSIONAL_PROFILE_ENABLED);

			// if NONE or BOTH are enabled, do not send anything (as it means the same thing)
			if (requirePersonalProfile && !requireProfessionalProfile) {
				AuthnContextClassRef profileRequirement = samlHelperService.buildSAMLObject(AuthnContextClassRef.class);
				requestedAuthnContext.getAuthnContextClassRefs().add(profileRequirement);

				profileRequirement.setAuthnContextClassRef("https://data.gov.dk/eid/Person");
				
				elementsPresent = true;
			}
			else if (!requirePersonalProfile && requireProfessionalProfile) {
				AuthnContextClassRef profileRequirement = samlHelperService.buildSAMLObject(AuthnContextClassRef.class);
				requestedAuthnContext.getAuthnContextClassRefs().add(profileRequirement);

				profileRequirement.setAuthnContextClassRef("https://data.gov.dk/eid/Professional");
				elementsPresent = true;				
			}
			
			if (elementsPresent) {
				authnRequest.setRequestedAuthnContext(requestedAuthnContext);
			}
		}

		return authnRequest;
	}

	public String getConsumerEndpoint(AuthnRequest authnRequest) throws InternalException {
		try {
			String assertionConsumerServiceURL = authnRequest.getAssertionConsumerServiceURL();

			if (!StringUtils.hasLength(assertionConsumerServiceURL)) {
				assertionConsumerServiceURL = sPMetadataService.getSSODescriptor().getDefaultAssertionConsumerService().getLocation();
			}

			return assertionConsumerServiceURL;
		}
		catch (Exception ex) {
			throw new InternalException("Unable to retrieve AssertionConsumerURL", ex);
		}
	}
}
