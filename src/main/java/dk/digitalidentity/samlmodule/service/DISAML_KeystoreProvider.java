package dk.digitalidentity.samlmodule.service;

import java.security.KeyStore;

public interface DISAML_KeystoreProvider {
	public default String getPrimaryKeystoreAlias() { return null; }
	public KeyStore getPrimaryKeystore();
	public String getPrimaryKeystorePassword();
	public default String getSecondaryKeystoreAlias() { return null; }
	public KeyStore getSecondaryKeystore();
	public String getSecondaryKeystorePassword();
}
