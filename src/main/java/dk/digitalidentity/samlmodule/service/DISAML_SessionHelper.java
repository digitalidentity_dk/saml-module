package dk.digitalidentity.samlmodule.service;

import javax.servlet.http.HttpServletRequest;

import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.LogoutRequest;
import org.opensaml.saml.saml2.core.impl.AuthnRequestMarshaller;
import org.opensaml.saml.saml2.core.impl.AuthnRequestUnmarshaller;
import org.opensaml.saml.saml2.core.impl.LogoutRequestMarshaller;
import org.opensaml.saml.saml2.core.impl.LogoutRequestUnmarshaller;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.w3c.dom.Element;

import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.SessionException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DISAML_SessionHelper {

	// Boolean
	public boolean getBoolean(SessionConstant key) throws SessionException {
		return getBoolean(key, false);
	}

	public boolean getBoolean(SessionConstant key, boolean defaultValue) throws SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();

		Object attribute = httpServletRequest.getSession().getAttribute(key.getKey());
		boolean bool = (boolean) (attribute != null ? attribute : defaultValue);

		if (log.isDebugEnabled()) {
			log.debug("getBoolean: Returned attribute with key=" + key.getKey() + ", defaultValue=" + defaultValue);
		}

		return bool;
	}

	public void setBoolean(SessionConstant key, boolean value) throws SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();
		httpServletRequest.getSession().setAttribute(key.getKey(), value);

		if (log.isDebugEnabled()) {
			log.debug("setBoolean: Set attribute with key=" + key.getKey() + " to value=" + value);
		}
	}

	// String
	public String getString(SessionConstant key) throws SessionException {
		return getString(key, null);
	}

	public String getString(SessionConstant key, String defaultValue) throws SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();

		Object attribute = httpServletRequest.getSession().getAttribute(key.getKey());
		String str = (String) (attribute != null ? attribute : defaultValue);

		if (log.isDebugEnabled()) {
			log.debug("getString: Returned attribute with key=" + key.getKey() + ", defaultValue=" + (defaultValue != null ? defaultValue : "<null>"));
		}

		return str;
	}

	public void setString(SessionConstant key, String value) throws SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();
		httpServletRequest.getSession().setAttribute(key.getKey(), value);

		if (log.isDebugEnabled()) {
			log.debug("setBoolean: Set attribute with key=" + key.getKey() + " to value=" + (value != null ? value : "<null>"));
		}
	}

	public AuthnRequest getAuthnRequest() throws InternalException, SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();

		Object attribute = httpServletRequest.getSession().getAttribute(SessionConstant.AUTHN_REQUEST.getKey());
		if (attribute == null) {
			if (log.isDebugEnabled()) {
				log.debug("getAuthnRequest: AuthnRequest was <null>");
			}
			return null;
		}

		try {
			Element marshalledAuthnRequest = (Element) attribute;
			AuthnRequest authnRequest = (AuthnRequest) new AuthnRequestUnmarshaller().unmarshall(marshalledAuthnRequest);
			if (log.isDebugEnabled()) {
				log.debug("getAuthnRequest: AuthnRequest found. ID=" + authnRequest.getID());
			}

			return authnRequest;
		} catch (UnmarshallingException ex) {
			throw new InternalException("Kunne ikke afkode login forespørgsel, Fejl url ikke kendt", ex);
		}
	}

	public void setAuthnRequest(AuthnRequest authnRequest) throws InternalException, SessionException {
		HttpServletRequest httpServletRequest = getServletRequest();

		if (authnRequest == null) {
			httpServletRequest.getSession().setAttribute(SessionConstant.AUTHN_REQUEST.getKey(), null);
			if (log.isDebugEnabled()) {
				log.debug("setAuthnRequest: AuthnRequest set to <null>");
			}

			return;
		}

		if (log.isDebugEnabled()) {
			log.debug("Saving AuthnRequest to session");
		}
		try {
			Element marshalledObject = new AuthnRequestMarshaller().marshall(authnRequest);
			httpServletRequest.getSession().setAttribute(SessionConstant.AUTHN_REQUEST.getKey(), marshalledObject);
			if (log.isDebugEnabled()) {
				log.debug("setAuthnRequest: AuthnRequest set. ID=" + authnRequest.getID());
			}
		} catch (MarshallingException ex) {
			throw new InternalException("Kunne ikke omforme login forespørgsel (AuthnRequest)", ex);
		}
	}

	public String getRelayState() throws SessionException {
		return getString(SessionConstant.RELAY_STATE);
	}

	public void setRelayState(String relayState) throws SessionException {
		setString(SessionConstant.RELAY_STATE, relayState);
	}

	private HttpServletRequest getServletRequest() throws SessionException{
		try {
			RequestAttributes attribs = RequestContextHolder.getRequestAttributes();

			if (attribs instanceof ServletRequestAttributes) {
				HttpServletRequest httpServletRequest = ((ServletRequestAttributes) attribs).getRequest();

				if (log.isDebugEnabled()) {
					log.debug("Getting HTTPServletRequest. SessionID=" + httpServletRequest.getSession().getId() + ", Thread=" + Thread.currentThread().getName());
				}

				return httpServletRequest;
			}
		}
		catch (Exception ex) {
			throw new SessionException("Could not get ServletRequest", ex);
		}
		throw new SessionException("Could not get ServletRequest");
	}

	// LogoutRequest
	public LogoutRequest getLogoutRequest() throws InternalException {
		HttpServletRequest httpServletRequest = getServletRequest();

		Object attribute = httpServletRequest.getSession().getAttribute(SessionConstant.LOGOUT_REQUEST.getKey());
		if (attribute == null) {
			if (log.isDebugEnabled()) {
				log.debug("getLogoutRequest: LogoutRequest was <null>");
			}
			return null;
		}

		try {
			Element marshalledLogoutRequest = (Element) attribute;
			LogoutRequest logoutRequest = (LogoutRequest) new LogoutRequestUnmarshaller().unmarshall(marshalledLogoutRequest);
			if (log.isDebugEnabled()) {
				log.debug("getLogoutRequest: LogoutRequest found. ID=" + logoutRequest.getID());
			}

			return logoutRequest;
		}
		catch (UnmarshallingException ex) {
			throw new InternalException("Kunne ikke afkode logout forespørgsel (LogoutRequest)", ex);
		}
	}

	public void setLogoutRequest(LogoutRequest logoutRequest) throws InternalException {
		HttpServletRequest httpServletRequest = getServletRequest();

		if (logoutRequest == null) {
			httpServletRequest.getSession().setAttribute(SessionConstant.LOGOUT_REQUEST.getKey(), null);
			if (log.isDebugEnabled()) {
				log.debug("setLogoutRequest: LogoutRequest set to <null>");
			}

			return;
		}

		try {
			Element marshall = new LogoutRequestMarshaller().marshall(logoutRequest);
			httpServletRequest.getSession().setAttribute(SessionConstant.LOGOUT_REQUEST.getKey(), marshall);
			if (log.isDebugEnabled()) {
				log.debug("setLogoutRequest: LogoutRequest set. ID=" + logoutRequest.getID());
			}
		}
		catch (MarshallingException ex) {
			throw new InternalException("Kunne ikke omforme logout forespørgsel (LogoutRequest)", ex);
		}
	}



	// Session end management
	public void logout() throws InternalException {
		logout(null);
	}

	public void logout(LogoutRequest logoutRequest) throws InternalException {
		// Delete everything not needed for logout procedure
		HttpServletRequest httpServletRequest = getServletRequest();
		for (SessionConstant constant : SessionConstant.values()) {
			if (constant.isDeleteOnLogout()) {
				httpServletRequest.getSession().removeAttribute(constant.getKey());
			}
		}

		// Save LogoutRequest to session if one is provided
		if (logoutRequest != null) {
			setLogoutRequest(logoutRequest);
		}

		if (log.isDebugEnabled()) {
			log.debug("Session: deleteOnLogout attributes removed");
		}
	}

	public void error() throws InternalException {
		HttpServletRequest httpServletRequest = getServletRequest();
		for (SessionConstant constant : SessionConstant.values()) {
			if (constant.isDeleteOnError()) {
				httpServletRequest.getSession().removeAttribute(constant.getKey());
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Session: deleteOnError attributes removed");
		}
	}

	public void login() throws InternalException {
		HttpServletRequest httpServletRequest = getServletRequest();
		for (SessionConstant constant : SessionConstant.values()) {
			if (constant.isDeleteOnLogin()) {
				httpServletRequest.getSession().removeAttribute(constant.getKey());
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Session: deleteOnLogin attributes removed");
		}
	}

	public void invalidateSAMLSession() throws SessionException {
		// We don't want to invalidate the whole session, if the application using this framework has stored data in it.
		// So we just remove all keys associated with the SAML Module
		HttpServletRequest httpServletRequest = getServletRequest();
		for (SessionConstant constant : SessionConstant.values()) {
			httpServletRequest.getSession().removeAttribute(constant.getKey());
		}

		if (log.isDebugEnabled()) {
			log.debug("SAMLModule session data removed");
		}
	}

	public void removeAttribute(SessionConstant key) throws SessionException {
		if (log.isDebugEnabled()) {
			log.debug("Removing attribute from session with key=" + key.getKey());
		}

		HttpServletRequest httpServletRequest = getServletRequest();
		httpServletRequest.getSession().removeAttribute(key.getKey());
	}
}
