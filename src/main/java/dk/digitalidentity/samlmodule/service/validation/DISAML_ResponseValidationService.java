package dk.digitalidentity.samlmodule.service.validation;

import javax.servlet.http.HttpServletRequest;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.handler.MessageHandlerException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.binding.security.impl.MessageLifetimeSecurityHandler;
import org.opensaml.saml.common.binding.security.impl.ReceivedEndpointSecurityHandler;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.Response;
import org.opensaml.saml.saml2.core.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

import java.util.Objects;

@Slf4j
@Service
public class DISAML_ResponseValidationService {

	@Autowired
	private DISAML_IdPMetadataService idPmetadataService;
	
	@Autowired
	private DISAML_Configuration configuration;

	public void validate(HttpServletRequest httpServletRequest, MessageContext<SAMLObject> messageContext, AuthnRequest authnRequest) throws InternalException, ExternalException {
		log.debug("Started validation of Response");

		Response response = (Response) messageContext.getMessage();
		if (response == null) {
			throw new ExternalException("Message did not contain a correct Response Object");
		}

		if (authnRequest == null && !configuration.isAllowIdPInitiatedLogin()) {
			throw new InternalException("No authnRequest to compare InResponseTo against");
		}

		validateDestination(httpServletRequest, messageContext);
		validateLifeTime(messageContext);
		if (configuration.getSp().isValidateInResponseTo() && authnRequest != null) {
			validateInResponseTo(response, authnRequest);
		}
		validateIssuer(response);
		validateStatus(response);

		log.debug("Completed validation of Response");
	}

	@SuppressWarnings("unchecked")
	private void validateDestination(HttpServletRequest httpServletRequest, MessageContext<SAMLObject> messageContext) throws InternalException, ExternalException {
		log.debug("Validating destination");

		ReceivedEndpointSecurityHandler endpointSecurityHandler = null;
		try {
			endpointSecurityHandler = new ReceivedEndpointSecurityHandler();
			endpointSecurityHandler.setHttpServletRequest(httpServletRequest);
			endpointSecurityHandler.initialize();
			endpointSecurityHandler.invoke(messageContext);
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize ReceivedEndpointSecurityHandler", e);
		}
		catch (MessageHandlerException e) {
			throw new ExternalException("Destination incorrect", e);
		}
		finally {
			if (endpointSecurityHandler != null && endpointSecurityHandler.isInitialized() && !endpointSecurityHandler.isDestroyed()) {
				endpointSecurityHandler.destroy();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void validateLifeTime(MessageContext<SAMLObject> messageContext) throws InternalException, ExternalException {
		log.debug("Validating Lifetime");

		MessageLifetimeSecurityHandler lifetimeHandler = null;
		try {
			lifetimeHandler = new MessageLifetimeSecurityHandler();
			lifetimeHandler.setClockSkew(60 * 5 * 1000);
			lifetimeHandler.initialize();
			lifetimeHandler.invoke(messageContext);
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize MessageLifetimeSecurityHandler", e);
		}
		catch (MessageHandlerException e) {
			throw new ExternalException("Message lifetime incorrect", e);
		}
		finally {
			if (lifetimeHandler != null && lifetimeHandler.isInitialized() && !lifetimeHandler.isDestroyed()) {
				lifetimeHandler.destroy();
			}
		}
	}

	private void validateInResponseTo(Response response, AuthnRequest authnRequest) throws ExternalException {
		log.debug("Validating InResponseTo");

		if (!Objects.equals(authnRequest.getID(), response.getInResponseTo())) {
			throw new ExternalException("InResponseTo does not match AuthnRequest ID. Expected: " + authnRequest.getID() + " Was: " + response.getInResponseTo());
		}
	}

	private void validateIssuer(Response response) throws InternalException, ExternalException {
		log.debug("Validating Issuer");

		Issuer issuer = response.getIssuer();
		if (issuer == null) {
			throw new ExternalException("No Issuer found");
		}

		String metadataEntityID = idPmetadataService.getMetadata(issuer).getEntityID();

		if (!Objects.equals(metadataEntityID, issuer.getValue())) {
			throw new ExternalException("Issuer does not match IdP metadata. Expected: " + metadataEntityID + " Was: " + issuer.getValue());
		}
	}

	private void validateStatus(Response response) throws ExternalException {
		if (response.getStatus() == null || response.getStatus().getStatusCode() == null) {
			throw new ExternalException("Response status or Response status statuscode was null");
		}

		if (!StatusCode.SUCCESS.equals(response.getStatus().getStatusCode().getValue())) {
			throw new ExternalException("Response status code is not Success. Expected: " + StatusCode.SUCCESS + " Was: " + response.getStatus().getStatusCode().getValue());
		}
	}
}