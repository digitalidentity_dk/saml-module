package dk.digitalidentity.samlmodule.service.validation;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.enums.NSISLevel;
import dk.digitalidentity.samlmodule.service.DISAML_TokenUserService;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.NSISLevelTooLowException;

import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.handler.MessageHandlerException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.binding.security.impl.MessageLifetimeSecurityHandler;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.NameID;
import org.opensaml.saml.saml2.core.RequestedAuthnContext;
import org.opensaml.saml.saml2.core.Subject;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.security.credential.UsageType;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.xmlsec.signature.support.SignatureException;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

@Slf4j
@Service
public class DISAML_AssertionValidationService {

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_TokenUserService tokenUserService;

	public void validate(MessageContext<SAMLObject> messageContext, Assertion assertion, AuthnRequest authnRequest) throws InternalException, ExternalException, NSISLevelTooLowException {
		log.debug("Started validation of Assertion");

		validateIssuer(assertion);
		validateSignature(assertion);
		validateLifeTime(messageContext);
		validateSubjectNameID(assertion);

		if (authnRequest == null && !configuration.isAllowIdPInitiatedLogin()) {
			throw new InternalException("No authnRequest to compare NSIS Level against against");
		}

		if (authnRequest != null) {
			validateNSISLevel(assertion, authnRequest);
		}

		log.debug("Completed validation of Assertion");
	}

	private void validateNSISLevel(Assertion assertion, AuthnRequest authnRequest) throws ExternalException, NSISLevelTooLowException {
		log.debug("Validating NSIS Level");
		NSISLevel requiredLevel = null;

		RequestedAuthnContext requestedAuthnContext = authnRequest.getRequestedAuthnContext();
		if (requestedAuthnContext == null || requestedAuthnContext.getAuthnContextClassRefs() == null || requestedAuthnContext.getAuthnContextClassRefs().isEmpty()) {
			return; // no ContextClassRefs on AuthnRequest, no validation necessary
		}

		for (AuthnContextClassRef authnContextClassRef : requestedAuthnContext.getAuthnContextClassRefs()) {
			String contextClassRef = authnContextClassRef.getAuthnContextClassRef();
			if (!StringUtils.hasLength(contextClassRef)) {
				continue;
			}

			if ("https://data.gov.dk/concept/core/nsis/loa/High".equals(contextClassRef)) {
				requiredLevel = NSISLevel.HIGH;
				break;
			}
			else if ("https://data.gov.dk/concept/core/nsis/loa/Substantial".equals(contextClassRef)) {
				requiredLevel = NSISLevel.SUBSTANTIAL;
				break;
			}
			else if ("https://data.gov.dk/concept/core/nsis/loa/Low".equals(contextClassRef)) {
				requiredLevel = NSISLevel.LOW;
				break;
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Required NSIS Level evaluated to " + (requiredLevel != null ? requiredLevel.toClaimValue() : "<null>"));
		}

		if (requiredLevel == null) {
			return;
		}

		HashMap<String, Object> attributeMap = new HashMap<>();
		tokenUserService.extractAttributes(assertion, attributeMap);

		if (!attributeMap.containsKey("https://data.gov.dk/concept/core/nsis/loa")) {
			throw new NSISLevelTooLowException("An NSIS Level was required by the AuthnRequest, but none were supplied in the Assertion");
		}

		Object loaObj = attributeMap.get("https://data.gov.dk/concept/core/nsis/loa");
		if (!(loaObj instanceof String)) {
			throw new ExternalException("An NSIS Level was required by the AuthnRequest, but an incorrect value were supplied in the Assertion");
		}

		NSISLevel givenLevel = NSISLevel.fromClaimValue((String) loaObj);
		if (givenLevel == null) {
			throw new ExternalException("An NSIS Level was required by the AuthnRequest, but an incorrect value were supplied in the Assertion");
		}

		if (!requiredLevel.equalOrLesser(givenLevel)) {
			throw new ExternalException("Insufficient NSIS Level");
		}

	}

	private void validateSignature(Assertion assertion) throws InternalException, ExternalException {
		log.debug("Validating Signature");

		if (!assertion.isSigned()) {
			throw new ExternalException("Assertion is not signed");
		}

		// Get Signing credential
		IdentityProvider identityProvider = idPMetadataService.getIdentityProvider(assertion.getIssuer());
		List<X509Certificate> x509Certificates = idPMetadataService.getX509Certificates(identityProvider, UsageType.SIGNING);

		boolean signatureValid = false;
		Exception ex = null;
		for (X509Certificate x509Certificate : x509Certificates) {
			BasicX509Credential credential = new BasicX509Credential(x509Certificate);
	
			// Validate Signature
			try {
				x509Certificate.checkValidity();
				SignatureValidator.validate(assertion.getSignature(), credential);
				
				signatureValid = true;
				break;
			}
			catch (SignatureException | CertificateNotYetValidException | CertificateExpiredException e) {
				ex = e;
			}
		}
		
		if (!signatureValid) {
			if (ex != null) {
				throw new ExternalException("Could not validate assertion signature", ex);
			}
			
			throw new ExternalException("Could not validate assertion signature");
		}
	}

	@SuppressWarnings("unchecked")
	private void validateLifeTime(MessageContext<SAMLObject> messageContext) throws InternalException, ExternalException {
		log.debug("Validating Lifetime");

		MessageLifetimeSecurityHandler lifetimeHandler = null;
		try {
			lifetimeHandler = new MessageLifetimeSecurityHandler();
			lifetimeHandler.setClockSkew(60 * 5 * 1000L);
			lifetimeHandler.initialize();
			lifetimeHandler.invoke(messageContext);
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize MessageLifetimeSecurityHandler", e);
		}
		catch (MessageHandlerException e) {
			throw new ExternalException("Message lifetime incorrect", e);
		}
		finally {
			if (lifetimeHandler != null && lifetimeHandler.isInitialized() && !lifetimeHandler.isDestroyed()) {
				lifetimeHandler.destroy();
			}
		}
	}

	private void validateIssuer(Assertion assertion) throws InternalException, ExternalException {
		log.debug("Validating Issuer");


		Issuer issuer = assertion.getIssuer();
		if (issuer == null) {
			throw new ExternalException("No Issuer found");
		}

		if (issuer.getValue() == null) {
			throw new ExternalException("Issuer was null");
		}

		EntityDescriptor metadata = idPMetadataService.getMetadata(issuer);
		if (metadata == null) {
			throw new ExternalException("No IdentityProvider matching Issuer");
		}

		String metadataEntityID = metadata.getEntityID();
		if (!Objects.equals(metadataEntityID, issuer.getValue())) {
			throw new ExternalException("Issuer does not match IdP metadata. Expected: " + metadataEntityID + " Was: " + issuer.getValue());
		}
	}

	private void validateSubjectNameID(Assertion assertion) throws ExternalException {
		log.debug("Validating Subject");

		Subject subject = assertion.getSubject();
		if (subject == null) {
			throw new ExternalException("No Subject found on Assertion");
		}

		NameID nameID = subject.getNameID();
		if (nameID == null) {
			throw new ExternalException("No Subject.NameID found on Assertion");
		}

		if (!StringUtils.hasLength(nameID.getValue())) {
			throw new ExternalException("Assertion.Subject.NameID was null or empty");
		}
	}
}