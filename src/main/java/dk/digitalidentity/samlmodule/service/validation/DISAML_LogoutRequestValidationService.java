package dk.digitalidentity.samlmodule.service.validation;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.handler.MessageHandlerException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.binding.security.impl.MessageLifetimeSecurityHandler;
import org.opensaml.saml.common.binding.security.impl.ReceivedEndpointSecurityHandler;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.LogoutRequest;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.security.credential.UsageType;
import org.opensaml.security.crypto.SigningUtil;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.xmlsec.algorithm.AlgorithmSupport;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

@Slf4j
@Service
public class DISAML_LogoutRequestValidationService {

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	public void validate(HttpServletRequest request, MessageContext<SAMLObject> messageContext, IdentityProvider identityProvider) throws InternalException, ExternalException {
		log.debug("Started validation of LogoutRequest");

		LogoutRequest logoutRequest = (LogoutRequest) messageContext.getMessage();
		if (logoutRequest == null) {
			throw new ExternalException("Message did not contain a correct LogoutRequest Object");
		}

		EntityDescriptor metadata = idPMetadataService.getMetadata(identityProvider);
		List<PublicKey> publicKeys = idPMetadataService.getPublicKeys(identityProvider, UsageType.SIGNING);

		validateIssuer(logoutRequest, metadata.getEntityID());
		validateDestination(request, messageContext);
		validateLifeTime(messageContext);
		validateSignature(request, publicKeys, logoutRequest, identityProvider);
		validateSessionIndex(logoutRequest);

		log.debug("Completed validation of LogoutRequest");
	}

	@SuppressWarnings("unchecked")
	private void validateDestination(HttpServletRequest httpServletRequest, MessageContext<SAMLObject> messageContext) throws InternalException, ExternalException {
		log.debug("Validating destination");

		ReceivedEndpointSecurityHandler endpointSecurityHandler = null;
		try {
			endpointSecurityHandler = new ReceivedEndpointSecurityHandler();
			endpointSecurityHandler.setHttpServletRequest(httpServletRequest);
			endpointSecurityHandler.initialize();
			endpointSecurityHandler.invoke(messageContext);
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize ReceivedEndpointSecurityHandler", e);
		}
		catch (MessageHandlerException e) {
			throw new ExternalException("Destination incorrect", e);
		}
		finally {
			if (endpointSecurityHandler != null && endpointSecurityHandler.isInitialized() && !endpointSecurityHandler.isDestroyed()) {
				endpointSecurityHandler.destroy();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void validateLifeTime(MessageContext<SAMLObject> messageContext) throws InternalException, ExternalException {
		log.debug("Validating Lifetime");

		MessageLifetimeSecurityHandler lifetimeHandler = null;
		try {
			lifetimeHandler = new MessageLifetimeSecurityHandler();
			lifetimeHandler.setClockSkew(60 * 5 * 1000);
			lifetimeHandler.initialize();
			lifetimeHandler.invoke(messageContext);
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize MessageLifetimeSecurityHandler", e);
		}
		catch (MessageHandlerException e) {
			throw new ExternalException("Message lifetime incorrect", e);
		}
		finally {
			if (lifetimeHandler != null && lifetimeHandler.isInitialized() && !lifetimeHandler.isDestroyed()) {
				lifetimeHandler.destroy();
			}
		}
	}

	private void validateSessionIndex(LogoutRequest logoutRequest) throws ExternalException {
		log.debug("Validating SessionIndex");

		if (logoutRequest.getSessionIndexes().size() != 1) {
			throw new ExternalException("Could not find single sessionIndex in Request");
		}
		if (!StringUtils.hasLength(logoutRequest.getSessionIndexes().get(0).getSessionIndex())) {
			throw new ExternalException("SessionIndex is empty");
		}
	}

	private void validateIssuer(LogoutRequest logoutRequest, String metadataEntityID) throws ExternalException {
		log.debug("Validating Issuer");

		Issuer issuer = logoutRequest.getIssuer();
		if (issuer == null) {
			throw new ExternalException("No Issuer found");
		}

		if (!Objects.equals(metadataEntityID, issuer.getValue())) {
			throw new ExternalException("Issuer does not match Metadata. Expected: " + metadataEntityID + " Was: " + issuer.getValue());
		}
	}

	private void validateSignature(HttpServletRequest request, List<PublicKey> publicKeys, LogoutRequest logoutRequest, IdentityProvider identityProvider) throws ExternalException, InternalException {
		log.debug("Validating Signature");

		// Select signature validation strategy based on Http-Redirect/Http-Post binding
		switch (request.getMethod()) {
			case "GET":
				String queryString = request.getQueryString();
				String signature = request.getParameter("Signature");
				String sigAlg = request.getParameter("SigAlg");

				if (!validateDetachedSignature(queryString, publicKeys, signature, sigAlg)) {
					throw new ExternalException("LogoutRequest Signature incorrect");
				}

				break;
			case "POST":
				List<X509Certificate> x509Certificates = idPMetadataService.getX509Certificates(identityProvider, UsageType.SIGNING);
				
				boolean validSignature = false;
				for (X509Certificate x509Certificate : x509Certificates) {
					BasicX509Credential credential = new BasicX509Credential(x509Certificate);

					try {
						Objects.requireNonNull(logoutRequest.getSignature(), "No signature present on logoutRequest for HTTP POST binding");
						SignatureValidator.validate(logoutRequest.getSignature(), credential);
						
						validSignature = true;
					}
					catch (Exception ex) {
						log.warn("Could not validate LogoutRequest signature", ex);
					}
				}
				
				if (!validSignature) {
					throw new ExternalException("LogoutRequest Signature incorrect");
				}
				
				break;
			default:
				throw new ExternalException("Could not validate signature, Wrong request method");
		}
	}

	private boolean validateDetachedSignature(String queryString, List<PublicKey> publicKeys, String signature, String sigAlg) throws ExternalException {
		// Get url string to be verified
		byte[] data = new byte[0];
		data = parseSignedQueryString(queryString).getBytes(StandardCharsets.UTF_8);

		// Decode signature
		byte[] decodedSignature = Base64.getDecoder().decode(signature);
		String jcaAlgorithmID = AlgorithmSupport.getAlgorithmID(sigAlg);

		boolean validSignature = false;

		for (PublicKey publicKey : publicKeys) {
			try {
				if (SigningUtil.verify(publicKey, jcaAlgorithmID, decodedSignature, data)) {
					validSignature = true;
					break;
				}
			}
			catch (Exception ex) {
				log.warn("Invalid signature on LogoutRequest", ex);
			}
		}
		
		return validSignature;
	}

	private String parseSignedQueryString(String queryString) {
		StringBuilder s = new StringBuilder();

		String samlRequestOrResponse = getParameter("SAMLRequest", queryString);
		String relayState = getParameter("RelayState", queryString);
		String sigAlg = getParameter("SigAlg", queryString);

		s.append("SAMLRequest");
		s.append("=");
		s.append(samlRequestOrResponse);

		if (relayState != null) {
			s.append("&");
			s.append("RelayState");
			s.append("=");
			s.append(relayState);
		}

		s.append("&");
		s.append("SigAlg");
		s.append("=");
		s.append(sigAlg);

		return s.toString();
	}

	private String getParameter(String name, String url) {
		String[] parameters = url.split("&");

		for (String parameter : parameters) {
			int pos = parameter.indexOf('=');
			String key = parameter.substring(0, pos);

			if (name.equals(key)) {
				return parameter.substring(pos + 1);
			}
		}

		return null;
	}
}
