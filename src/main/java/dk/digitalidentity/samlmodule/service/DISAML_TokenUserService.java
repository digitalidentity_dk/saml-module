package dk.digitalidentity.samlmodule.service;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.core.xml.schema.XSAny;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.Attribute;
import org.opensaml.saml.saml2.core.AttributeStatement;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.NameID;
import org.opensaml.saml.saml2.core.impl.AssertionMarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.CompactToken;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.model.PrivilegeList;
import dk.digitalidentity.samlmodule.model.SamlGrantedAuthority;
import dk.digitalidentity.samlmodule.model.SamlIdentityProviderProvider;
import dk.digitalidentity.samlmodule.model.SamlLoginPostProcessor;
import dk.digitalidentity.samlmodule.model.TokenUser;
import dk.digitalidentity.samlmodule.service.saml.DISAML_OpenSAMLHelperService;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DISAML_TokenUserService {

	@Autowired(required = false)
	private SamlLoginPostProcessor postProcesser;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_OpenSAMLHelperService samlHelperService;

	@Autowired(required = false)
	private SamlIdentityProviderProvider identityProviderProvider;

	public boolean isAuthenticated() {
		if (SecurityContextHolder.getContext().getAuthentication() != null &&
				SecurityContextHolder.getContext().getAuthentication().getDetails() != null &&
				SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof TokenUser) {
			return true;
		}

		return false;
	}

	public TokenUser getTokenUser() {
		if (!isAuthenticated()) {
			return null;
		}

		return (TokenUser) SecurityContextHolder.getContext().getAuthentication().getDetails();
	}

	public Object loadUserBySAML(Assertion assertion) throws UsernameNotFoundException {
		ArrayList<SamlGrantedAuthority> authorities = new ArrayList<>();
		Map<String, Object> attributes = new HashMap<>();

		NameID nameID = assertion.getSubject().getNameID(); // This value is present (Validated on received assertion)
		String username = nameID.getValue();
		String cvr = "";
		String rawToken = null;
		
		try {
			if (configuration.isStoreRawToken()) {
				rawToken = tokenToRawString(assertion);
			}
			else {
				logAssertion(assertion);
			}

			cvr = extractCvr(assertion);

			extractAttributes(assertion, attributes);

			extractRolesFromOioBpp(assertion, authorities);

			extractRolesFromClaim(assertion, authorities);
		}
		catch (Exception ex) {
			log.error("Bad or missing token", ex);
		}

		TokenUser tokenUser = TokenUser.builder()
				.authorities(authorities)
				.rawToken(rawToken)
				.cvr(cvr)
				.attributes(attributes)
				.username(username)
				.build();

		// allow the postProcesser to give it a spin
		if (postProcesser != null) {
			postProcesser.process(tokenUser);
		}

		// Establish session
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(tokenUser.getUsername(), "N/A", tokenUser.getAuthorities());
		authToken.setDetails(tokenUser);

		SecurityContextHolder.getContext().setAuthentication(authToken);

		return tokenUser;
	}

	public void logout() {
		SecurityContextHolder.clearContext();
	}

	public void extractAttributes(Assertion assertion, Map<String, Object> attributes) {
		if (assertion.getSubject() != null) {
			String name = getX509NameIdValue("CN", assertion.getSubject().getNameID());
			String uuid = getX509NameIdValue("Serial", assertion.getSubject().getNameID());

			attributes.put(TokenUser.ATTRIBUTE_NAME, name);
			attributes.put(TokenUser.ATTRIBUTE_UUID, uuid);
		}

		if (assertion.getAttributeStatements() != null) {
			for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {
				if (attributeStatement.getAttributes() == null || attributeStatement.getAttributes().isEmpty()) {
					continue;
				}

				for (Attribute attribute : attributeStatement.getAttributes()) {
					if (attribute.getAttributeValues() == null || attribute.getAttributeValues().isEmpty()) {
						continue;
					}

					// TODO Feature: The old saml-module seems to assume 1 attributeValue per Attribute
					//  		     (or at least overwrites the value to be equal to the last value in the list)
					// 				 Maybe we should support Lists of values?
					XMLObject valueObj = attribute.getAttributeValues().get(0);
					attributes.put(attribute.getName(), ((XSAny) valueObj).getTextContent());
				}
			}
		}
	}

	// in discovery mode, use the configured CVR, otherwise to to extact from NameID, and fallback to empty
	private String extractCvr(Assertion assertion) {
		if (configuration.getIdp().isDiscovery() && identityProviderProvider != null) {
			Issuer issuer = assertion.getIssuer();

			if (issuer != null && StringUtils.hasLength(issuer.getValue())) {
				String value = issuer.getValue();
				IdentityProvider idp = identityProviderProvider.getByEntityId(value);

				if (idp != null) {
					return idp.getCvr();
				}
			}
		}

		// if the supplied identity provider does not supply a CVR, do a lookup in the issued Subject
		if (assertion.getSubject() != null) {
			return getX509NameIdValue("O", assertion.getSubject().getNameID());
		}

		return "";
	}

	// if the token contains an OIO-BPP block, we will parse it for roles
	private void extractRolesFromOioBpp(Assertion assertion, ArrayList<SamlGrantedAuthority> authorities) throws InternalException {
		if (assertion.getAttributeStatements() != null) {
			for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {
				if (attributeStatement.getAttributes() == null) {
					continue;
				}
				Map<String, String> attributeMap = samlHelperService.extractAttributeValues(attributeStatement);

				// Check if attributes contains the key used for OIO BPP roles
				if (!attributeMap.containsKey("dk:gov:saml:attribute:Privileges_intermediate")) {
					continue;
				}

				// Found oioBPPString, converting to SamlGrantedAuthorities
				String oioBPPString = attributeMap.get("dk:gov:saml:attribute:Privileges_intermediate");
				byte[] oiobppRawBytes = Base64.getDecoder().decode(oioBPPString);
				try {
					JAXBContext jaxbContext = JAXBContext.newInstance(PrivilegeList.class);
					Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
					Reader reader = new InputStreamReader(new ByteArrayInputStream(oiobppRawBytes));
					PrivilegeList oioBPP = (PrivilegeList) unmarshaller.unmarshal(reader);

					for (PrivilegeList.PrivilegeGroup privilegeGroup : oioBPP.getPrivilegeGroup()) {
						SamlGrantedAuthority authority = null;

						ArrayList<SamlGrantedAuthority.Constraint> constraints = null;
						if (privilegeGroup.getConstraint() != null) {
							constraints = new ArrayList<>();
							for (PrivilegeList.Constraint constraint : privilegeGroup.getConstraint()) {
								constraints.add(new SamlGrantedAuthority.Constraint(constraint.getName(), constraint.getValue()));
							}
						}

						if (privilegeGroup.getPrivilege() != null) {
							for (PrivilegeList.Privilege privilege : privilegeGroup.getPrivilege()) {
								// Guild SamlGrantedAuthority
								SamlGrantedAuthority.SamlGrantedAuthorityBuilder builder = SamlGrantedAuthority.builder();

								// Always set role
								builder.authority("ROLE_" + privilege.getValue());

								// Constraints
								if (privilegeGroup.getConstraint() != null) {
									builder.constraints(constraints);
								}

								// Scope
								if (StringUtils.hasLength(privilegeGroup.getScope())) {
									builder.scope(privilegeGroup.getScope());
								}

								// Add authority
								authority = builder.build();
								authorities.add(authority);
							}
						}
					}
				}
				catch (JAXBException e) {
					throw new InternalException(e);
				}
			}
		}
	}

	// if a claim is configured, extract roles from that claim (if present)
	private void extractRolesFromClaim(Assertion assertion, ArrayList<SamlGrantedAuthority> authorities) {
		if (!StringUtils.hasLength(configuration.getClaims().getRoleClaimName())) {
			return;
		}

		List<AttributeStatement> attributeStatements = assertion.getAttributeStatements();
		if (attributeStatements == null || attributeStatements.isEmpty()) {
			return;
		}

		for (AttributeStatement attributeStatement : attributeStatements) {
			List<Attribute> attributes = attributeStatement.getAttributes();
			if (attributes == null || attributes.isEmpty()) {
				continue;
			}

			for (Attribute attribute : attributes) {
				if (!configuration.getClaims().getRoleClaimName().equalsIgnoreCase(attribute.getName())) {
					continue;
				}

				// RoleClaimName found, now extract roles
				List<XMLObject> attributeValues = attribute.getAttributeValues();
				if (attributeValues != null) {
					for (XMLObject attributeValue : attributeValues) {
						SamlGrantedAuthority authority = new SamlGrantedAuthority("ROLE_" + attributeValue.getDOM().getTextContent());
						authorities.add(authority);
					}
				}
			}
		}
	}

	private static String getX509NameIdValue(String field, NameID nameId) {
		if (nameId == null || !StringUtils.hasLength(nameId.getValue()) || !Objects.equals(nameId.getFormat(), NameID.X509_SUBJECT)) {
			return null;
		}

		String nameIdVal = nameId.getValue();

		StringBuilder builder = new StringBuilder();

		int idx = nameIdVal.indexOf(field + "=");
		if (idx >= 0) {
			for (int i = idx + field.length() + 1; i < nameIdVal.length(); i++) {
				if (nameIdVal.charAt(i) == ',') {
					break;
				}

				builder.append(nameIdVal.charAt(i));
			}
		}

		return builder.toString();
	}

	private String tokenToRawString(Assertion assertion) throws MarshallingException {
		AssertionMarshaller marshaller = new AssertionMarshaller();
		Element element = marshaller.marshall(assertion);

		StringWriter writer = new StringWriter();

		DOMImplementation domImpl = element.getOwnerDocument().getImplementation();
		DOMImplementationLS domImplLS = (DOMImplementationLS) domImpl.getFeature("LS", "3.0");

		LSOutput serializerOut = domImplLS.createLSOutput();
		serializerOut.setCharacterStream(writer);

		LSSerializer serializer = domImplLS.createLSSerializer();
		serializer.getDomConfig().setParameter("xml-declaration", false);
		serializer.getDomConfig().setParameter("format-pretty-print", false);
		serializer.write(element, serializerOut);

		return writer.toString();
	}
	
	private void logAssertion(Assertion assertion) {
		try {
			switch (configuration.getSp().getAssertionLogging()) {
				case FULL:
					logFullToken(assertion);
					break;
				case COMPACT:
					logCompactToken(assertion);
					break;
				case NONE:
				default:
					break;
			}
		}
		catch (Exception ex) {
			log.error("Failed to log token", ex);
		}
	}

	private void logFullToken(Assertion assertion) throws MarshallingException {
		AssertionMarshaller marshaller = new AssertionMarshaller();
		Element element = marshaller.marshall(assertion);

		StringWriter writer = new StringWriter();

		DOMImplementation domImpl = element.getOwnerDocument().getImplementation();
		DOMImplementationLS domImplLS = (DOMImplementationLS) domImpl.getFeature("LS", "3.0");

		LSOutput serializerOut = domImplLS.createLSOutput();
		serializerOut.setCharacterStream(writer);

		LSSerializer serializer = domImplLS.createLSSerializer();
		serializer.getDomConfig().setParameter("xml-declaration", false);
		serializer.getDomConfig().setParameter("format-pretty-print", true);
		serializer.write(element, serializerOut);

		log.info("Full token =\n" + writer.toString());
	}

	private void logCompactToken(Assertion assertion) throws JsonProcessingException {
		CompactToken token = new CompactToken();

		if (assertion.getSubject() != null && assertion.getSubject().getNameID() != null) {
			token.setNameId(assertion.getSubject().getNameID().getValue());
		}

		token.setCvr(extractCvr(assertion));

		if (StringUtils.hasLength(configuration.getClaims().getRoleClaimName())) {
			// If RoleClaim is set up, extract it into a comma-separated list of roles for logging
			List<AttributeStatement> attributeStatements = assertion.getAttributeStatements();
			if (attributeStatements == null || attributeStatements.isEmpty()) {
				return;
			}

			for (AttributeStatement attributeStatement : attributeStatements) {
				List<Attribute> attributes = attributeStatement.getAttributes();
				if (attributes == null || attributes.isEmpty()) {
					continue;
				}

				for (Attribute attribute : attributes) {
					if (!configuration.getClaims().getRoleClaimName().equalsIgnoreCase(attribute.getName())) {
						continue;
					}

					StringBuilder builder = new StringBuilder();

					for (XMLObject role : attribute.getAttributeValues()) {
						if (builder.length() > 0) {
							builder.append(",");
						}

						builder.append(role.getDOM().getTextContent());
					}

					token.setRoles(builder.toString());
				}
			}
		}
		else {
			// If no RoleClaim is not set up, extract OIO BPP
			if (assertion.getAttributeStatements() != null) {
				for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {
					if (attributeStatement.getAttributes() == null) {
						continue;
					}
					Map<String, String> attributeMap = samlHelperService.extractAttributeValues(attributeStatement);

					// Check if attributes contains the key used for OIO BPP roles
					if (!attributeMap.containsKey("dk:gov:saml:attribute:Privileges_intermediate")) {
						continue;
					}

					// Found oioBPPString
					String oiobppAttribute = attributeMap.get("dk:gov:saml:attribute:Privileges_intermediate");
					token.setRoles(new String(Base64.getDecoder().decode(oiobppAttribute), StandardCharsets.UTF_8));
					break; // OIOBPP found and set on CompactToken, break for loop
				}
			}
		}

		log.info("Compact token = " + new ObjectMapper().writeValueAsString(token));
	}
}
