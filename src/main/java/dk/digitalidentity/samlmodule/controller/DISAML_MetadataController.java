package dk.digitalidentity.samlmodule.controller;

import java.io.StringWriter;

import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml.saml2.metadata.impl.EntityDescriptorMarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import dk.digitalidentity.samlmodule.service.metadata.DISAML_SPMetadataService;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;

@RestController
@EnableCaching
public class DISAML_MetadataController {

	@Autowired
	private DISAML_SPMetadataService serviceProviderMetadataService;

	@ResponseBody
	@Cacheable(value = "DISAML_SP_Metadata")
	@GetMapping(value = { "${di.saml.pages.prefix:/saml}" + "/metadata", "${di.saml.pages.prefix:/saml}" + "/metadata/**" }, produces = MediaType.APPLICATION_XML_VALUE)
	public String getSPMetadata() throws InternalException {
		// Get Metadata
		EntityDescriptor metadata = serviceProviderMetadataService.getMetadata();

		// Marshall and send EntityDescriptor
		try {
			EntityDescriptorMarshaller entityDescriptorMarshaller = new EntityDescriptorMarshaller();
			Element element = entityDescriptorMarshaller.marshall(metadata);

			StringWriter writer = new StringWriter();

			DOMImplementation domImpl = element.getOwnerDocument().getImplementation();
			DOMImplementationLS domImplLS = (DOMImplementationLS) domImpl.getFeature("LS", "3.0");

			LSOutput serializerOut = domImplLS.createLSOutput();
			serializerOut.setCharacterStream(writer);

			LSSerializer serializer = domImplLS.createLSSerializer();
			serializer.write(element, serializerOut);

			return writer.toString();
		}
		catch (MarshallingException e) {
			throw new InternalException("Kunne ikke omforme metadata", e);
		}
	}
}
