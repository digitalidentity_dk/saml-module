package dk.digitalidentity.samlmodule.controller;

import java.io.IOException;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.encoder.MessageEncodingException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.saml2.binding.encoding.impl.HTTPRedirectDeflateEncoder;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.core.LogoutRequest;
import org.opensaml.saml.saml2.core.LogoutResponse;
import org.opensaml.saml.saml2.core.Response;
import org.opensaml.saml.saml2.core.Status;
import org.opensaml.saml.saml2.core.StatusCode;
import org.opensaml.saml.saml2.core.StatusMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.service.DISAML_LoggingService;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.service.DISAML_TokenUserService;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.service.saml.DISAML_LogoutRequestService;
import dk.digitalidentity.samlmodule.service.saml.DISAML_LogoutResponseService;
import dk.digitalidentity.samlmodule.util.LoggingConstants;
import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.SingleLogoutException;
import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

@Slf4j
@Controller
public class DISAML_LogoutController {

	@Autowired
	private DISAML_LogoutRequestService logoutRequestService;

	@Autowired
	private DISAML_LogoutResponseService logoutResponseService;

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@Autowired
	private DISAML_LoggingService loggingService;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	@Autowired
	private DISAML_TokenUserService tokenUserService;

	@Autowired
	private DISAML_Configuration configuration;

	@RequestMapping(value = "${di.saml.pages.prefix:/saml}" + "${di.saml.pages.logoutMetadataPath:/logout}")
	public void logoutRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws SingleLogoutException {
		if (log.isTraceEnabled()) {
			log.trace("logoutRequest endpoint called");
		}

		try {
			// Determine if caller has a SAMLRequest (already in singleLogout flow)
			// OR if the caller is attempting to start a logout flow
			if (StringUtils.hasLength(httpServletRequest.getParameter("SAMLRequest"))) {
				if (log.isDebugEnabled()) {
					log.debug("SAMLRequest found on request. Decoding LogoutRequest object");
				}

				handleLogoutRequest(httpServletRequest, httpServletResponse);
			}
			else {
				if (log.isDebugEnabled()) {
					log.debug("SAMLRequest not found on request. Starting logout flow");
				}

				handleInitializeLogout(httpServletResponse, tokenUserService.isAuthenticated());
			}
		}
		catch (ExternalException | InternalException ex) {
			// We wrap every exception from the logout controller with this exception
			// to stop it from throwing session related errors and send a warn instead
			throw new SingleLogoutException(ex);
		}
	}

	private void handleInitializeLogout(HttpServletResponse httpServletResponse, boolean authenticated) throws InternalException, ExternalException {
		// If the user landed on the Logout endpoint while not being logged in
		// we should not send any saml logout messages but should just show the user the logout page
		if (!authenticated) {
			try {
				// Removing any information for good measure
				removeTokenUserAndSessionData();

				// Redirecting user to logout page
				httpServletResponse.sendRedirect(configuration.getPages().getLogout());
				return;
			}
			catch (IOException e) {
				throw new InternalException(e);
			}
		}

		// Create LogoutRequest

		// when starting a logout request we check the session to find out which Identity Provider was used for the initial login
		// and pass the logout request to it
		String entityId = sessionHelper.getString(SessionConstant.CHOSEN_IDENTITY_PROVIDER);
		if (entityId == null) {
			// No IdP has been chosen for login, assume we are logged out
			sessionHelper.invalidateSAMLSession();
			
			try {
				// Removing any information for good measure
				removeTokenUserAndSessionData();

				// Redirect to logout success endpoint
				httpServletResponse.sendRedirect(configuration.getPages().getLogout());
				return;
			}
			catch (IOException e) {
				throw new InternalException("Kunne ikke videresende bruger til logout side efter færdigt logout");
			}
		}

		IdentityProvider identityProvider = idPMetadataService.getIdentityProvider(entityId);
		MessageContext<SAMLObject> logoutRequestMessage = logoutRequestService.createLogoutRequest(identityProvider);
		LogoutRequest logoutRequest = (LogoutRequest) logoutRequestMessage.getMessage();

		// Remove data from session and save LogoutRequest on session
		removeTokenUserAndSessionData(logoutRequest);

		// Send LogoutRequest to the IdP
		// Deflating and sending the message
		HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
		encoder.setMessageContext(logoutRequestMessage);
		encoder.setHttpServletResponse(httpServletResponse);

		loggingService.logLogoutRequest(logoutRequest, LoggingConstants.OUTGOING);
		try {
			encoder.initialize();
			encoder.encode();
		}
		catch (ComponentInitializationException | MessageEncodingException e) {
			throw new InternalException("Failed sending LogoutRequest", e);
		}
	}

	private void removeTokenUserAndSessionData() {
		removeTokenUserAndSessionData(null);
	}

	private void removeTokenUserAndSessionData(@Nullable LogoutRequest logoutRequest) {
		try {
			sessionHelper.logout(logoutRequest);
		}
		catch (InternalException ex) {
			log.warn("An error occurred while cleaning session", ex);
		}

		tokenUserService.logout();
	}

	private void handleLogoutRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws InternalException, ExternalException {
		// Extract LogoutRequest from request and log it.
		// If we cannot extract the request we have to show an error page
		MessageContext<SAMLObject> messageContext = logoutRequestService.getMessageContext(httpServletRequest);
		LogoutRequest logoutRequest = logoutRequestService.getLogoutRequest(messageContext);
		loggingService.logLogoutRequest(logoutRequest, LoggingConstants.INCOMING);

		// Validate LogoutRequest.
		// If the validation does not go well we log a warning
		// and continue on with responding to the request
		try {
			IdentityProvider identityProvider = idPMetadataService.getIdentityProvider(logoutRequest.getIssuer());
			logoutRequestService.validateLogoutRequest(httpServletRequest, messageContext, identityProvider);
		}
		catch (InternalException | ExternalException ex) {
			log.warn("Validation of incoming LogoutRequest failed", ex);
		}

		// Remove data from session and save LogoutRequest on session
		removeTokenUserAndSessionData(logoutRequest);

		// Create LogoutResponse
		// If we fail in creating or sending a logout response we have to show an error page
		MessageContext<SAMLObject> logoutResponseMessage = logoutResponseService.createLogoutResponse(logoutRequest);
		LogoutResponse logoutResponse = (LogoutResponse) logoutResponseMessage.getMessage();

		// Send LogoutResponse to the IdP
		// Deflating and sending the message
		HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
		encoder.setMessageContext(logoutResponseMessage);
		encoder.setHttpServletResponse(httpServletResponse);

		loggingService.logLogoutResponse(logoutResponse, LoggingConstants.OUTGOING);
		try {
			encoder.initialize();
			encoder.encode();
		}
		catch (ComponentInitializationException | MessageEncodingException e) {
			throw new InternalException("Failed sending LogoutResponse", e);
		}
	}

	@RequestMapping(value = "${di.saml.pages.prefix:/saml}" + "${di.saml.pages.logoutMetadataPath:/logout}" + "/response")
	public void logoutResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws SingleLogoutException {
		try {
			// Get LogoutRequest by session
			LogoutRequest logoutRequest = sessionHelper.getLogoutRequest();

			// Receive and parse LogoutResponse
			MessageContext<SAMLObject> messageContext = logoutResponseService.getMessageContext(httpServletRequest);

			// Handle receiving a normal Response instead of a LogoutResponse in case of errors
			if (messageContext.getMessage() instanceof Response) {
				handleResponseInsteadOfLogoutResponse((Response) messageContext.getMessage());
			}

			LogoutResponse logoutResponse = logoutResponseService.getLogoutResponse(messageContext);
			loggingService.logLogoutResponse(logoutResponse, LoggingConstants.INCOMING);

			// Validate signature
			Issuer issuer = logoutResponse.getIssuer();
			if (issuer == null || !StringUtils.hasLength(issuer.getValue())) {
				throw new ExternalException("Kunne ikke validere LogoutResponse da issuer ikke var sat");
			}

			IdentityProvider identityProvider = idPMetadataService.getIdentityProvider(issuer);
			logoutResponseService.validateLogoutResponse(httpServletRequest, messageContext, logoutRequest, identityProvider);

			// Redirect to logout success endpoint
			sessionHelper.invalidateSAMLSession();
			try {
				httpServletResponse.sendRedirect(configuration.getPages().getLogout());
			}
			catch (IOException e) {
				throw new InternalException("Kunne ikke vidresende bruger til logout side efter færdigt logout");
			}
		}
		catch (ExternalException | InternalException ex) {
			// We wrap every exception from the logout controller with this exception
			// to stop it from throwing session related errors and send a warn instead
			throw new SingleLogoutException(ex);
		}
	}

	private void handleResponseInsteadOfLogoutResponse(Response response) throws InternalException, ExternalException {
		// Extract information from status object
		Status status = response.getStatus();
		if (status != null) {
			// Get status code, might suggest who is the responsible party for the error
			String statusCodeValue = StatusCode.REQUESTER; // Default to requester error
			StatusCode statusCode = status.getStatusCode();
			if (statusCode != null && StatusCode.RESPONDER.equals(statusCode.getValue())) {
				statusCodeValue = StatusCode.RESPONDER;
			}

			// Get error message
			StatusMessage statusMessageObj = status.getStatusMessage();
			if (statusMessageObj != null) {
				String statusMessage = statusMessageObj.getMessage();
				if (StringUtils.hasLength(statusMessage)) {
					if (StatusCode.REQUESTER.equals(statusCodeValue)) {
						throw new InternalException(statusMessage);
					}
					else {
						throw new ExternalException(statusMessage);
					}
				}
			}
		}
		throw new InternalException("Kan ikke læse LogoutResponse");
	}
}
