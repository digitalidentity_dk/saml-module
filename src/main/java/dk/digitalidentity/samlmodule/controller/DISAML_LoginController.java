package dk.digitalidentity.samlmodule.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.digitalidentity.samlmodule.model.enums.NSISLevel;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.encoder.MessageEncodingException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.saml2.binding.encoding.impl.HTTPRedirectDeflateEncoder;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.AuthnStatement;
import org.opensaml.saml.saml2.core.Response;
import org.opensaml.saml.saml2.core.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.model.IdentityProvider;
import dk.digitalidentity.samlmodule.service.DISAML_LoggingService;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.service.DISAML_TokenUserService;
import dk.digitalidentity.samlmodule.service.metadata.DISAML_IdPMetadataService;
import dk.digitalidentity.samlmodule.service.saml.DISAML_AssertionService;
import dk.digitalidentity.samlmodule.service.saml.DISAML_AuthnRequestService;
import dk.digitalidentity.samlmodule.service.validation.DISAML_AssertionValidationService;
import dk.digitalidentity.samlmodule.service.validation.DISAML_ResponseValidationService;
import dk.digitalidentity.samlmodule.util.LoggingConstants;
import dk.digitalidentity.samlmodule.util.SessionConstant;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.NSISLevelTooLowException;
import lombok.extern.slf4j.Slf4j;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

@Slf4j
@Controller
public class DISAML_LoginController {

	@Autowired
	private DISAML_AssertionService assertionService;

	@Autowired
	private DISAML_ResponseValidationService responseValidationService;

	@Autowired
	private DISAML_AssertionValidationService assertionValidationService;

	@Autowired
	private DISAML_LoggingService loggingService;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	@Autowired
	private DISAML_TokenUserService tokenUserService;

	@Autowired
	private DISAML_AuthnRequestService authnRequestService;

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_IdPMetadataService idPMetadataService;

	@GetMapping(value = "${di.saml.pages.prefix:/saml}" + "${di.saml.pages.loginMetadataPath:/login}")
	public ModelAndView initializeLoginFlow(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Model model, @RequestParam(required = false, name = "idp") String entityId) throws InternalException, ExternalException {
		if (log.isTraceEnabled()) {
			log.trace("initializeLoginFlow endpoint called");
		}

		if (configuration.getIdp().isDiscovery()) {
			if (!StringUtils.hasLength(entityId)) {
				// If discovery is enabled and no specific identityProvider has been supplied redirect to applications own choose IdP Page
				return new ModelAndView("redirect:" + configuration.getPages().getChooseIdentityProvider());
			}
		}

		IdentityProvider identityProvider;
		try {
			identityProvider = idPMetadataService.getIdentityProvider(entityId);
		}
		catch (ExternalException ex) {
			// External Exception is thrown in case that the system does not know the IdP selected, we should just let the user/system retry
			log.warn("IdentityProvider was not known, retrying", ex);

			return new ModelAndView("redirect:" + configuration.getPages().getChooseIdentityProvider());
		}

		// Save chosen IdP for later
		sessionHelper.setString(SessionConstant.CHOSEN_IDENTITY_PROVIDER, identityProvider.getEntityId());

		String neededNSISLevel = sessionHelper.getString(SessionConstant.NEEDED_NSIS_LEVEL);
		NSISLevel nsisLevel = NSISLevel.fromClaimValue(neededNSISLevel);

		// if the identity Provider cannot be prompted for NSIS Level yet an NSIS level is needed
		if (!identityProvider.isContextClassRefEnabled() && nsisLevel != null && NSISLevel.NONE != nsisLevel) {
			log.warn("The chosen Identity Provider does not support sending contextClassRef, but a specific NSIS level is required - we may not get what we expect back");
		}

		MessageContext<SAMLObject> authnRequestMessage = authnRequestService.createAuthnRequest(identityProvider);
		AuthnRequest authnRequest = (AuthnRequest) authnRequestMessage.getMessage();

		// Log and save authnRequest before sending
		loggingService.logAuthnRequest(authnRequest, LoggingConstants.OUTGOING);
		sessionHelper.setAuthnRequest(authnRequest);

		// Send assertion
		HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
		encoder.setHttpServletResponse(httpServletResponse);
		encoder.setMessageContext(authnRequestMessage);

		try {
			encoder.initialize();
			encoder.encode();
		}
		catch (ComponentInitializationException | MessageEncodingException e) {
			throw new InternalException("Encoding error", e);
		}

		return null;
	}

	@PostMapping(value = "${di.saml.pages.prefix:/saml}" + "${di.saml.pages.loginMetadataPath:/login}")
	public void assertionConsumer(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws InternalException, ExternalException, NSISLevelTooLowException {
		if (log.isTraceEnabled()) {
			log.trace("assertionConsumer endpoint called");
		}

		// get AuthnRequest and RelayState by session
		AuthnRequest authnRequest = sessionHelper.getAuthnRequest();
		
		// not used for anything yet, but this is how we get it, if we need it
		//String relayState = sessionHelper.getRelayState();

		// verify that a response is present
		String samlResponseParameter = httpServletRequest.getParameter("SAMLResponse");
		if (!StringUtils.hasLength(samlResponseParameter)) {
			log.warn("Post against /login but no SAMLResponse present for outgoing authnRequest.ID = " + ((authnRequest != null) ? authnRequest.getID() : "<null>"));
			throw new ExternalException("Der er ikke noget svar fra login-tjenesten");
        }
		
		// Extract Response object
		MessageContext<SAMLObject> messageContext = assertionService.getMessageContext(httpServletRequest);
		Response response = assertionService.getResponse(messageContext);
		loggingService.logResponse(response, LoggingConstants.INCOMING);

		// Check for StatusCode other than success
		if (!StatusCode.SUCCESS.equals(response.getStatus().getStatusCode().getValue())) {
			String errMsg = "Response status code was not successful. ";

			if (response.getStatus().getStatusMessage() != null) {
				String message = response.getStatus().getStatusMessage().getMessage();
				if (StringUtils.hasLength(message)) {
					errMsg += "Status message: '" + message + "'";
				}
			}

			throw new ExternalException(errMsg);
		}

		// Validate Response
		responseValidationService.validate(httpServletRequest, messageContext, authnRequest);

		// Extract and validate Assertion
		Assertion assertion = assertionService.getAssertion(response);
		loggingService.logAssertion(assertion, LoggingConstants.INCOMING);
		assertionValidationService.validate(messageContext, assertion, authnRequest);

		// Assertion is validated, the user is now authenticated
		// Start login procedure
		tokenUserService.loadUserBySAML(assertion);

		// Save Session index
		List<AuthnStatement> authnStatements = assertion.getAuthnStatements();
		if (authnStatements != null) {
			Optional<String> sessionIndexOpt = authnStatements.stream().map(AuthnStatement::getSessionIndex).findFirst();
			if (sessionIndexOpt.isPresent()) {
				sessionHelper.setString(SessionConstant.SESSION_INDEX, sessionIndexOpt.get());
			}
		}
		if (sessionHelper.getString(SessionConstant.SESSION_INDEX) == null) {
			log.warn("No SessionIndex was found on assertion, Continuing login, but SLO might be affected");
		}

		// Get desired page and remove now-unnecessary stuff from session
		String desiredPage = sessionHelper.getString(SessionConstant.DESIRED_PAGE);
		sessionHelper.login();

		// Send user to page
		try {
			if (configuration.getPages().isDesiredPageEnabled() && StringUtils.hasLength(desiredPage)) {
				httpServletResponse.sendRedirect(desiredPage);
			}
			else if (StringUtils.hasLength(configuration.getPages().getSuccess())) {
				httpServletResponse.sendRedirect(configuration.getPages().getSuccess());
			}
			else {
				httpServletResponse.sendRedirect("/");
			}
		}
		catch (IOException ex) {
			throw new InternalException("Kunne ikke videresende bruger efter succesfuldt login", ex);
		}
	}
}
