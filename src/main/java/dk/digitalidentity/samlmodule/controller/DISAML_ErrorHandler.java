package dk.digitalidentity.samlmodule.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import dk.digitalidentity.samlmodule.util.exceptions.NSISLevelTooLowException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.WebAttributes;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.util.ErrorConstant;
import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice(basePackages = "dk.digitalidentity.samlmodule.controller")
public class DISAML_ErrorHandler {

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	@ExceptionHandler(value = { Exception.class })
	public RedirectView handleSAMLErrors(Exception ex, HttpServletRequest httpServletRequest, HttpServletResponse response) {
		// this method handles ALL errors in the controllers.

		// determine the type of the error
		String type = "";

		if (ex instanceof ExternalException) {
			type = "Identity Provider Fejl";
		}
		else if (ex instanceof NSISLevelTooLowException) {
			type = "Sikringsniveau (NSIS) ikke højt nok";
		}
		else {
			type = "Tjenesteudbyder fejl";
		}

		// log the error as WARN (no reason to trigger alarm on this)
		log.warn("SAML error", ex);

		// send Redirect to configured error endpoint
		RedirectView redirect = new RedirectView(configuration.getPages().getError());
		FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(httpServletRequest);
		if (outputFlashMap != null){
			outputFlashMap.put(ErrorConstant.TYPE.toString(), type);
			outputFlashMap.put(ErrorConstant.EXCEPTION.toString(), ex);

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			String stacktrace = sw.toString();
			outputFlashMap.put(ErrorConstant.STACKTRACE.toString(), stacktrace);
			outputFlashMap.put(ErrorConstant.TIMESTAMP.toString(), LocalDateTime.now());
		}

		// make exception available to custom error handlers
		httpServletRequest.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, ex);

		// Try to delete session attributes that should be deleted on error
		try {
			sessionHelper.error();
		}
		catch (InternalException ignored) {
			;
		}

		return redirect;
	}
}
