package dk.digitalidentity.samlmodule.config;

import dk.digitalidentity.samlmodule.model.SamlIdentityProviderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import dk.digitalidentity.samlmodule.filter.SAMLFilter;
import dk.digitalidentity.samlmodule.service.DISAML_SessionHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class DISAML_SAMLFilterConfig {

	@Autowired
	private DISAML_Configuration configuration;

	@Autowired
	private DISAML_SessionHelper sessionHelper;

	@Autowired(required = false)
	private SamlIdentityProviderProvider identityProviderProvider;

	@Bean(name = "DISAML_SAMLFilter")
	public FilterRegistrationBean<SAMLFilter> samlFilter() {
		if (log.isTraceEnabled()) {
			log.trace("Configuring SAML Filter");
		}

		FilterRegistrationBean<SAMLFilter> registrationBean = new FilterRegistrationBean<>();

		SAMLFilter samlFilter = new SAMLFilter();
		samlFilter.setServices(configuration, sessionHelper, identityProviderProvider);

		registrationBean.setFilter(samlFilter);
		registrationBean.addUrlPatterns("/*");
		registrationBean.setOrder(2);

		return registrationBean;
	}
}
