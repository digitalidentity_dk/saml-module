package dk.digitalidentity.samlmodule.config.settings;

import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_ClaimConfiguration;
import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_IdPConfiguration;
import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_KeystoreConfiguration;
import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_PagesConfiguration;
import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_ProxyConfiguration;
import dk.digitalidentity.samlmodule.config.settings.modules.DISAML_SPConfiguration;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "di.saml")
public class DISAML_Configuration {
	private DISAML_SPConfiguration sp = new DISAML_SPConfiguration();
	private DISAML_IdPConfiguration idp = new DISAML_IdPConfiguration();
	private DISAML_PagesConfiguration pages = new DISAML_PagesConfiguration();
	private DISAML_ProxyConfiguration proxy = new DISAML_ProxyConfiguration();
	private DISAML_ClaimConfiguration claims = new DISAML_ClaimConfiguration();
	private DISAML_KeystoreConfiguration keystore = new DISAML_KeystoreConfiguration();
	
	private boolean acceptSelfSigned = false;
	private boolean storeRawToken = false;
	private boolean allowIdPInitiatedLogin = false;
}
