package dk.digitalidentity.samlmodule.config.settings.modules;

import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.model.enums.AssertionLogging;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DISAML_SPConfiguration {
	private String entityId;
	private String baseUrl;

	private String technicalContactEmail = "no-reply@domain.com";
	private boolean passive = false;
	private boolean forceAuthn = false;
	private String contentSecurityPolicy;
	private boolean validateInResponseTo = false;
	private AssertionLogging assertionLogging = AssertionLogging.FULL;

	public String getEntityId() {
		if (!StringUtils.hasLength(this.entityId)) {
			return this.baseUrl;
		}
		
		return this.entityId;
	}
}
