package dk.digitalidentity.samlmodule.config.settings.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DISAML_IdPConfiguration {
	private String metadataLocation;
	private boolean discovery = false;

	// enable contextclassref parameters
	private boolean contextClassRefEnabled = false;
	private boolean requirePersonProfile = true;

	// Delay is in hours
	private long resolverMaxRefreshDelay = 4L;
	private long resolverMinRefreshDelay = 1L;
	private boolean selectProfilesEnabled = false;
}
