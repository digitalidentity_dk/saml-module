package dk.digitalidentity.samlmodule.config.settings.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DISAML_ProxyConfiguration {
	private String contextPath = "";
}
