package dk.digitalidentity.samlmodule.config.settings.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DISAML_ClaimConfiguration {
	private String roleClaimName = "urn:roles";
}
