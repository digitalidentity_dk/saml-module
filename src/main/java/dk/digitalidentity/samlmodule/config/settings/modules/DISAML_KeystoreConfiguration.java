package dk.digitalidentity.samlmodule.config.settings.modules;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DISAML_KeystoreConfiguration {
	private String location;
	private String password;
	
	private String secondaryLocation;
	private String secondaryPassword;
}
