package dk.digitalidentity.samlmodule.config.settings.modules;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import dk.digitalidentity.samlmodule.model.enums.NSISLevel;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DISAML_PagesConfiguration {
	private String success = "/";
	private String error = "/error";
	private String logout = "/";
	private String chooseIdentityProvider = "/discovery";
	private String prefix = "/saml";
	private String loginMetadataPath = "/login";
	private String logoutMetadataPath = "/logout";
	private boolean csrfEnabled = true;
	private boolean desiredPageEnabled = true;
	private boolean strictRefererPolicyEnabled = true;
	private boolean xFrameOptionsDisabledForNonSecuredPages = true;
	private List<String> csrfBypass = List.of("/api/**", "/manage/**");
	private List<String> nonsecured = List.of("/", "/error", "/webjars/**", "/css/**", "/js/**", "/img/**", "/api/**", "/manage/**", "/favicon.ico");

	// first setting must be flipped to TRUE for any of the others to take effect
	private boolean nsisRequired = false;
	private NSISLevel nsisRequiredDefaultLevel = NSISLevel.SUBSTANTIAL;   // default NSIS Level if not mentioned in any of the lists below
	private List<String> nsisNotRequired = new ArrayList<>();             // list of endpoints that does NOT require NSIS, even if turned on
	private List<String> nsisLowRequired = new ArrayList<>();             // list of endpoints that overrules the default, and sets to LOW
	private List<String> nsisSubstantialRequired = new ArrayList<>();     // list of endpoints that overrules the default, and sets to SUBSTANTIAL
	private List<String> nsisHighRequired = new ArrayList<>();            // list of endpoints that overrules the default, and sets to HIGH
	private List<String> brokeredEndpoints = new ArrayList<>();           // list of endpoints that are brokered

	public String getPrefix() {
		String computedPrefix = "/saml";

		if (StringUtils.hasLength(this.prefix)) {
			computedPrefix = this.prefix;

			if (this.prefix.endsWith("/")) {
				computedPrefix = this.prefix.substring(0, this.prefix.length()-2);
			}

			if (!this.prefix.startsWith("/")) {
				computedPrefix = "/" + computedPrefix;
			}
		}

		return computedPrefix;
	}
}
