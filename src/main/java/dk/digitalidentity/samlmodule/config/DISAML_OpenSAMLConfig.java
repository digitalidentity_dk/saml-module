package dk.digitalidentity.samlmodule.config;

import org.opensaml.core.config.InitializationService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DISAML_OpenSAMLConfig implements ApplicationRunner {

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// This initializes openSAML
		InitializationService.initialize();
	}
}
