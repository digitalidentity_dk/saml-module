package dk.digitalidentity.samlmodule.config;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dk.digitalidentity.samlmodule.config.settings.DISAML_Configuration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class DISAML_HttpClientConfig {

	@Autowired
	private DISAML_Configuration configuration;

	@Bean(name = "DISAML_HTTPClient")
	public HttpClient httpClient() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		if (log.isTraceEnabled()) {
			log.trace("Configuring HTTP Client");
		}

		if (configuration.isAcceptSelfSigned()) {
			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
	
			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf)
					.register("http", new PlainConnectionSocketFactory())
					.build();

			PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
	
			return HttpClients.custom().addInterceptorFirst(interceptor()).setSSLSocketFactory(sslsf).setConnectionManager(connectionManager).build();
		}

		return HttpClients.custom().addInterceptorFirst(interceptor()).build();
	}
	
	// it happens that the other end is down - and we cannot really do anything about it, so we don't log it as an error unless it happens a lot
	@Bean
	public HttpResponseInterceptor interceptor() {
		return new HttpResponseInterceptor() {
			private int errorCounter = 0;

			@Override
			public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
				if (response.getStatusLine().getStatusCode() >= 400) {
					errorCounter++;
					
					// we refresh about 8 times in a day, so we give them a day to give us good data before we start logging errors
					if (errorCounter < 8) {
						log.warn("Failed to fetch data - HTTP " + response.getStatusLine().getStatusCode() + ", changing to HTTP 304");
						response.setStatusCode(HttpStatus.SC_NOT_MODIFIED);
					}
				}
				else {
					// successful, so reset errorCounter
					errorCounter = 0;
				}
			}
		};
	}
}
