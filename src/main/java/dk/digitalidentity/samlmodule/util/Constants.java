package dk.digitalidentity.samlmodule.util;

public class Constants {
	public static final String CONFIGURED_IDENTITY_PROVIDER = "CONFIGURED_IDENTITY_PROVIDER";
	public static final String LEVEL_OF_ASSURANCE = "https://data.gov.dk/concept/core/nsis/loa";
}
