package dk.digitalidentity.samlmodule.util;

public enum ErrorConstant {
	ERROR_ID, TYPE, EXCEPTION, STACKTRACE, TIMESTAMP
}
