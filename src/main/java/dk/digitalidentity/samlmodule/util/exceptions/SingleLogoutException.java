package dk.digitalidentity.samlmodule.util.exceptions;

public class SingleLogoutException extends Exception {
	private static final long serialVersionUID = 6588536664432016556L;

	public SingleLogoutException() {
	}

	public SingleLogoutException(String message) {
		super(message);
	}

	public SingleLogoutException(String message, Throwable cause) {
		super(message, cause);
	}

	public SingleLogoutException(Throwable cause) {
		super(cause);
	}
}

