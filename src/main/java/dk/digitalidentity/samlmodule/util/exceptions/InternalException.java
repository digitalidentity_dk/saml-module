package dk.digitalidentity.samlmodule.util.exceptions;

public class InternalException extends Exception {
	private static final long serialVersionUID = 4076082559779682951L;

	public InternalException() {
	}

	public InternalException(String message) {
		super(message);
	}

	public InternalException(String message, Throwable cause) {
		super(message, cause);
	}

	public InternalException(Throwable cause) {
		super(cause);
	}
}
