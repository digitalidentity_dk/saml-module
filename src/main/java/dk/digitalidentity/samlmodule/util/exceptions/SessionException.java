package dk.digitalidentity.samlmodule.util.exceptions;

public class SessionException extends InternalException {
	private static final long serialVersionUID = 8168385057874143837L;

	public SessionException() {
	}

	public SessionException(String message) {
		super(message);
	}

	public SessionException(String message, Throwable cause) {
		super(message, cause);
	}

	public SessionException(Throwable cause) {
		super(cause);
	}
}
