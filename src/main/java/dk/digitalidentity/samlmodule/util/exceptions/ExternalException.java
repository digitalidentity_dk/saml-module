package dk.digitalidentity.samlmodule.util.exceptions;

public class ExternalException extends Exception {
	private static final long serialVersionUID = 3259661098049858458L;

	public ExternalException() {
	}

	public ExternalException(String message) {
		super(message);
	}

	public ExternalException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExternalException(Throwable cause) {
		super(cause);
	}
}
