package dk.digitalidentity.samlmodule.util.exceptions;

public class NSISLevelTooLowException extends Exception {
	private static final long serialVersionUID = 6081225892954724446L;

	public NSISLevelTooLowException() {
	}

	public NSISLevelTooLowException(String message) {
		super(message);
	}

	public NSISLevelTooLowException(String message, Throwable cause) {
		super(message, cause);
	}

	public NSISLevelTooLowException(Throwable cause) {
		super(cause);
	}
}
