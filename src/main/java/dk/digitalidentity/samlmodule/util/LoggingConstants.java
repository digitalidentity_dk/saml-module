package dk.digitalidentity.samlmodule.util;

public class LoggingConstants {
	public static final String INCOMING = "Incoming";
	public static final String OUTGOING = "Outgoing";
}
