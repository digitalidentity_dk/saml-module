package dk.digitalidentity.samlmodule.util;

import lombok.Getter;

@Getter
public enum SessionConstant {
	AUTHN_REQUEST("DI_SAML_AUTHN_REQUEST", true, true, true),
	CHOSEN_IDENTITY_PROVIDER("CHOSEN_IDENTITY_PROVIDER", false, true, true),
	DESIRED_PAGE("DI_SAML_DESIRED_PAGE", true, true, true),
	LOGOUT_REQUEST("DI_SAML_LOGOUT_REQUEST", true, false, true),
	RELAY_STATE("DI_SAML_RELAY_STATE", false, true, true),
	NEEDED_NSIS_LEVEL("NEEDED_NSIS_LEVEL", true, true, true),
	SESSION_INDEX("DI_SAML_SESSION_INDEX", false, true, true),
	BROKERING_ENABLED("DI_SAML_BROKERING_ENABLED", false, true, true),
	PERSONAL_PROFILE_ENABLED("DI_SAML_PERSONAL_ENABLED", false, true, true),
	PROFESSIONAL_PROFILE_ENABLED("DI_SAML_PROFESSIONAL_ENABLED", false, true, true);

	private final String key;
	private final boolean deleteOnLogin;
	private final boolean deleteOnLogout;
	private final boolean deleteOnError;

	private SessionConstant(String key, boolean deleteOnLogin, boolean deleteOnLogout, boolean deleteOnError) {
		this.key = key;
		this.deleteOnLogin = deleteOnLogin;
		this.deleteOnLogout = deleteOnLogout;
		this.deleteOnError = deleteOnError;
	}
}