package dk.digitalidentity.samlmodule.util;

import javax.servlet.http.HttpServletRequest;

import dk.digitalidentity.samlmodule.util.exceptions.ExternalException;
import dk.digitalidentity.samlmodule.util.exceptions.InternalException;
import net.shibboleth.utilities.java.support.xml.BasicParserPool;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.decoder.MessageDecodingException;
import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.saml2.binding.decoding.impl.HTTPPostDecoder;
import org.opensaml.saml.saml2.binding.decoding.impl.HTTPRedirectDeflateDecoder;

import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import org.springframework.http.HttpMethod;

import java.util.Objects;

public class RequestDecodeUtil {

	public static MessageContext<SAMLObject> getMessageContext(HttpServletRequest request) throws InternalException, ExternalException {
		String method = request.getMethod().toLowerCase();

		if (Objects.equals(method, HttpMethod.GET.name().toLowerCase())) {
			return decodeGET(request);
		}
		else if (Objects.equals(method, HttpMethod.POST.name().toLowerCase())) {
			return decodePOST(request);
		}
		else {
			throw new ExternalException("Request method type not supported (was: " + method + ")");
		}
	}

	private static MessageContext<SAMLObject> decodeGET(HttpServletRequest request) throws InternalException, ExternalException {
		try {
			HTTPRedirectDeflateDecoder decoder = new HTTPRedirectDeflateDecoder();
			decoder.setHttpServletRequest(request);

			BasicParserPool parserPool = new BasicParserPool();
			parserPool.initialize();

			decoder.setParserPool(parserPool);
			decoder.initialize();
			decoder.decode();

			MessageContext<SAMLObject> msgContext = decoder.getMessageContext();
			decoder.destroy();

			return msgContext;
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize HTTPRedirectDeflateDecoder", e);
		}
		catch (MessageDecodingException e) {
			throw new ExternalException("Could not decode message", e);
		}
	}

	private static MessageContext<SAMLObject> decodePOST(HttpServletRequest request) throws InternalException, ExternalException {
		try {
			HTTPPostDecoder decoder = new HTTPPostDecoder();
			decoder.setHttpServletRequest(request);

			BasicParserPool parserPool = new BasicParserPool();
			parserPool.initialize();

			decoder.initialize();
			decoder.decode();

			MessageContext<SAMLObject> msgContext = decoder.getMessageContext();
			decoder.destroy();

			return msgContext;
		}
		catch (ComponentInitializationException e) {
			throw new InternalException("Could not initialize HTTPPostDecoder", e);
		}
		catch (MessageDecodingException e) {
			throw new ExternalException("Could not decode message", e);
		}
	}
}