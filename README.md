



# Required settings

| Setting                      | Description                                                  |
| ---------------------------- | ------------------------------------------------------------ |
| di.saml.sp.entityId          | **Required.** The service providers EntityID                 |
| di.saml.sp.baseUrl           | **Required.** The service providers Baseurl                  |
| di.saml.idp.metadataLocation | **Required if discovery is not enabled (default).** The location of the IdP's metadata. Prefixed by either "file:" or "url:" for fetching the metadata from those places. Can just be a string representation of the metadata |
| di.saml.keystore.location    | **Required.** location of JKS keystore                       |
| di.saml.keystore.password    | **Required.** password of entry in keystore                  |

# All settings

| setting                                  | default                                                      | description                                                  |
| ---------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| di.saml.sp.entityId                      |                                                              | **Required.** The service providers EntityID                 |
| di.saml.sp.baseUrl                       |                                                              | **Required.** The service providers Baseurl                  |
| di.saml.sp.technicalContactEmail         | no-reply@domain.com                                          | Email of the techical support for this service provider      |
| di.saml.sp.passive                       | false                                                        | Set to true to require passive login by the IdP              |
| di.saml.sp.forceAuthn                    | false                                                        | Set to true to force users to enter credentials when logging in (disable signle-signon basically) |
| di.saml.sp.contentSecurityPolicy         |                                                              | Sets the "Content-Security-Policy" header in http calls      |
| di.saml.sp.assertionLogging              | FULL                                                         | FULL/COMPACT/NONE Determines type of logging for assertions  |
| di.saml.sp.validateInResponseTo          | false                                                        | Set to true to validate that incoming responses matches the stored authnRequest |
| di.saml.idp.metadataLocation             |                                                              | **Required if discovery is not enabled.** The location of the IdP's metadata. Prefixed by either "file:" or "url:" for fetching the metadata from those places. Can just be a string representation of the metadata |
| di.saml.idp.contextClassRefEnabled       | false                                                        | Set to true to include AuthnContextClassRef in AuthnRequests |
| di.saml.idp.requireSubstantial           | true                                                         | Set to true alonside "contextClassRefEnabled" to send Substantial requirement |
| di.saml.idp.requirePersonProfile         | true                                                         | Set to true alonside "contextClassRefEnabled" to send person profile requirement |
| di.saml.idp.resolverMaxRefreshDelay      | 4                                                            | How long to keep IdP metadata before refreshing. in hours    |
| di.saml.idp.resolverMinRefreshDelay      | 1                                                            | How long to keep IdP metadata before refreshing. in hours    |
| di.saml.idp.discovery                    | false                                                        | Set to true to enable discovery. Remember to implement the Interface SamlIdentityProviderProvider |
| di.saml.pages.success                    | /                                                            | URL to dump the user at after login                          |
| di.saml.pages.error                      | /error                                                       | URL to dump the user at after a failed SAML operation        |
| di.saml.pages.logout                     | /                                                            | URL to dump the user at after a logout                       |
| di.saml.pages.chooseIdentityProvider     | /discovery                                                   | URL to dump the user if discovery is enabled and no IdP has been specified in call to /saml/login |
| di.saml.pages.prefix                     | /saml                                                        | Prefix used for all saml pages by the saml-module (/saml/login, /saml/logout...) |
| di.saml.pages.csrfEnabled                | true                                                         | Set to false if you need to disable CSRF protection          |
| di.saml.pages.csrfBypass                 | /api/\*\*,/manage/\*\*                                       | List of endpoints (same syntax as nonsecured.pages) for which no CSRF  protection is enabled even if the above setting is set to true |
| di.saml.pages.nonsecured                 | /,/error,/webjars/\*\*,/css/\*\*,/js/\*\*,/img/\*\*,/api/\*\*,/manage/\*\*,/favicon.ico | List of endpoints (either specific files or folders) that are not secured |
| di.saml.pages.xFrameOptionsDisabled      | false                                                        |                                                              |
| di.saml.pages.strictRefererPolicyEnabled | true                                                         |                                                              |
| di.saml.proxy.contextPath                |                                                              |                                                              |
| di.saml.claims.roleClaimName             | urn:roles                                                    | The name of the claim from where roles are read              |
| di.saml.keystore.location                |                                                              | **Required.** location of JKS keystore                       |
| di.saml.keystore.password                |                                                              | **Required.** password of entry in keystore                  |
| di.saml.acceptSelfSigned                 | false                                                        | Set to true to allow HttpClient to accept self signed certificates |
| di.saml.storeRawToken                    | false                                                        | Set to true to keep a copy of the full SAML Assertion on TokenUser |
| di.saml.allowIdPInitiatedLogin           | false                                                        | Set to true to allow IdP initiated login. This wil ignore missing AuthnRequests on session when recieving a Response |

# Releasing

## From bitbucket
Just create a tag with your wanted version, this will create a new release and upload it to S3.

## Locally
The following environment variables must be set before releasing.  
The AWS keys should have write access to the s3 public-maven-repository bucket.
```AWS_ACCESS_KEY_ID```
```AWS_SECRET_ACCESS_KEY```

To do the actual release run these commands:
````
```
RELEASE_VERSION=X.Y.Z ./mvnw publish
```
````